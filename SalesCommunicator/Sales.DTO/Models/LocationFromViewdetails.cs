﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class LocationFromViewdetails
    {
        public string Zone { get; set; }
        public string Location { get; set; }
        public string Customer { get; set; }
        public DateTime? DateTime { get; set; }
        public string Action { get; set; }
    }

    public class GetPostFavouritesCount
    {
        public int TotalPostFavoutites { get; set; }
        public double IncreaseFromLastWeek { get; set; }
        public int CurrentWeek { get; set; }
        public int LastWeek { get; set; }
    }

    public class GetPostReachCount
    {
        public int TotalReach { get; set; }
        public double IncreaseFromLastWeek { get; set; }
        public int CurrentWeek { get; set; }
        public int LastWeek { get; set; }
    }

    public class LocationFromView
    {
        public string Location { get; set; }
        public int NoOfViews { get; set; }
    }
}
