﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class Notification
    {
        public Int64 NotificationID { get; set; }
        public string PostNo { get; set; }
        public string UserId { get; set; }
        public DateTime? NotificationDate { get; set; }
        public string NotificationStatus { get; set; }
        public string NotificationText { get; set; }
    }

    public class GetNotification
    {
        public string UserId { get; set; }
    }

    public class UpdateNotification
    {
        //public int UserId { get; set; }
        public string UserId { get; set; }
        public Int64 NotificationID { get; set; }
    }
    public class DeleteNotification
    {
        public int UserId { get; set; }
        public Int64 NotificationID { get; set; }
    }
    public class DeleteOutputNotfy
    {
        public Int64 NotificationID { get; set; }
        public string NotificationStatus { get; set; }
    }

    public class UpdateOutputNotfy
    {
        public Int64 NotificationID { get; set; }
    }
    public class Count
    {
        public int CountNo { get; set; }
    }
}
