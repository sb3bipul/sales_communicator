﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class ZoneDto
    {
        public string Zone { get; set; }
        public string ZoneName { get; set; }
        public string CompanyID { get; set; }
    }
}
