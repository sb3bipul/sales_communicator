﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class DivisionCompanyDto
    {
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string DivisionName { get; set; }
    }
}
