﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class CompanyDto
    {
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
    }
}
