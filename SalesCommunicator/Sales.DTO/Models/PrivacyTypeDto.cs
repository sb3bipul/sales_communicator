﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class PrivacyTypeDto
    {
        public int ID { get; set; }
        public string PRIVACYTYPE { get; set; }
    }
}
