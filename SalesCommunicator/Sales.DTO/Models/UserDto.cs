﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class UserDto
    {
        public string Authorization { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Domain { get; set; }
        public string Role { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string UserType { get; set; }
        public string BrokerPicName { get; set; }
        public string IsNewReleaseFlag { get; set; }
        public string BrokerDivisionCode { get; set; }
        public string DeliveryCharge { get; set; }
        public string MinimumDeliveryChargeAmount { get; set; }
        public bool IsMappedUser { get; set; }
        public bool IsMappedUserDefaultCompany { get; set; }
        public string DefaultCompany { get; set; }
        public string Message { get; set; }
    }

    public class UserDetailsDto
    {
        public string UserName { get; set; }
        public string UserFullName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Domain { get; set; }
        public string Role { get; set; }
        public string RoleId { get; set; }
        public string UserRoleName { get; set; }
        public string CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string UserType { get; set; }
        public string BrokerPicName { get; set; }
        public string IsNewReleaseFlag { get; set; }
        public string LoginCompanyId { get; set; }
        public string BrokerDivisionCode { get; set; }
        public string DeliveryCharge { get; set; }
        public string MinimumDeliveryChargeAmount { get; set; }
        public bool IsCreatePost { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public int CountryId { get; set; }
        public string CountryName { get; set; }
    }
}
