﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class PostTypeDto
    {
        public int ID { get; set; }
        public string POSTTYPE { get; set; }
        public string DiscoverImage { get; set; }
    }
}
