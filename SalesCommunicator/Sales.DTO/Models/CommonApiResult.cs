﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class GenericResponse<T>
    {
        public string Domain { get; set; }
        public string Event { get; set; }
        public string ClientData { get; set; }
        public bool Status { get; set; }
        public T Payload { get; set; }
    }
}
