﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models
{
    public class UserPostDto
    {
        public string POSTNO { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime CREATEDON { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
    }

    public class PostDataDto
    {
        public string POSTNO { get; set; }
        public string CREATEDBY { get; set; }
        public string CREATEDON { get; set; }
        public string PRIVACYTYPE { get; set; }
        public string POSTTYPE { get; set; }
        public int PostTypeId { get; set; }
        public string COMMENTS { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public string POSTMEDIA { get; set; }
        public string Header { get; set; }
        public string Excelmedia { get; set; }
        public string Pdfmedia { get; set; }
        public string PPTmedia { get; set; }
        public Boolean IsFavorite { get; set; }
        public int CommentCount { get; set; }
    }

    public class PostDataMediaDto
    {
        public string POSTNO { get; set; }
        public string CREATEDBY { get; set; }
        public string CREATEDON { get; set; }
        public string PRIVACYTYPE { get; set; }
        public string POSTTYPE { get; set; }
        public int PostTypeId { get; set; }
        public string COMMENTS { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public string Header { get; set; }
        public List<PostMediaDto> POSTMEDIAS { get; set; }
        public string Excelmedia { get; set; }
        public string Pdfmedia { get; set; }
        public string PPTmedia { get; set; }
        public Boolean IsFavorite { get; set; }
        public int CommentCount { get; set; }
    }

    public class PostMediaDto
    {
        public string image { get; set; }
        public string thumbImage { get; set; }
        public string alt { get; set; }
        public string title { get; set; }
    }
}
