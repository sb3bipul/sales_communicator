﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models.RequestModel
{
    public class DivisionModel
    {
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
    }

    public class GroupModel
    {
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
    }
}
