﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models.RequestModel
{
    public class UserPostMediaModel
    {
        public string POSTNO { get; set; }
        public string MEDIAFILENAME { get; set; }
        //public string FILEURL { get; set; }
        public string MediaBase64 { get; set; }
        public string PdfBase64 { get; set; }
        public string pdfName { get; set; }
        public string excelFileName { get; set; }
        public string excelFileData { get; set; }
        public string pptFileName { get; set; }
        public string pptFileData { get; set; }
        public string ct { get; set; }
        public bool IsMobile { get; set; }
    }
}
