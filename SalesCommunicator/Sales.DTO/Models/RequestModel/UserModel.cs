﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models.RequestModel
{
    public class UserModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
    public class LogindBindingModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool isLoggedIn { get; set; }

    }
}
