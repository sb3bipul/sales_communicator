﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.DTO.Models.RequestModel
{
    public class UserPostModel
    {
        public string CREATEDBY { get; set; }
        public int PRIVACYTYPEID { get; set; }
        public int POSTTYPEID { get; set; }
        public string COMMENTS { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public string Header { get; set; }
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
        public string Zone { get; set; }
    }

    public class AddNotification
    {
        public string PostNo { get; set; }
        public string CREATEDBY { get; set; }
        public string NotificationText { get; set; }
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
        public string Zone { get; set; }
        public string TagUser { get; set; }
    }

    public class PostMediaModel
    {
        public string CREATEDBY { get; set; }
        public string PRIVACYTYPEID { get; set; }
        public string POSTTYPEID { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public string TagText { get; set; }
        public string UserName { get; set; }
        public string UserRole { get; set; }
        public bool IsFavorite { get; set; }
        //public int PageSize { get; set; }
        //public int PageNumber { get; set; }
    }

    public class UserPostLocation
    {
        public string POSTNO { get; set; }
        public string POSTLOCATION { get; set; }
    }

    public class UserPostTag
    {
        public string POstNo { get; set; }
        public string UserName { get; set; }
        public string TagLocation { get; set; }
    }

    public class SaveCammentCount
    {
        public string PostNo { get; set; }
        public string savecommentCountNo { get; set; }
    }

    public class NotificID
    {
        public string UserName { get; set; }
    }

    public class CommentPostNo
    {
        public string PostNo { get; set; }
    }

    public class CommentCount
    {
        public int commentContNo { get; set; }
    }

    public class GetUserListByZoneGroupDiv
    {
        public string Zone { get; set; }
        public string GroupId { get; set; }
        public string DivisionId { get; set; }
        public string PostNo { get; set; }
    }

    public class PostComment
    {
        public string PostNo { get; set; }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public string Comment { get; set; }
        public string CompanyId { get; set; }
    }

    public class PostCommentList
    {
        public string PostNo { get; set; }
        public string UserName { get; set; }
        public string UserFullName { get; set; }
    }

    public class PostCommenText
    {
        public string PostNo { get; set; }
        public string SenderUser { get; set; }
        public string ReceiverUser { get; set; }
        public string TextMsg { get; set; }
        public string UserFullName { get; set; }
    }
}
