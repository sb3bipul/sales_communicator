﻿using Sales.DTO.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Interfaces
{
    public interface INotificationService
    {
        Task<List<Notification>> GetNotification(GetNotification getNotification);
        Task<List<Notification>> InsertNotification(Notification notification);
        //Task<List<UpdateNotification>> UpdateNotification(UpdateNotification update);
        Task<List<UpdateNotification>> UpdateNotification(GetNotification update);
        Task<List<DeleteOutputNotfy>> DeleteNotification(DeleteNotification delete);
        //Task<List<Count>> GetCounts(GetNotification _count);
        Task<List<Count>> GetCounts(GetNotification _count);

    }
}
