﻿using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Interfaces
{
    public interface IUserService
    {
        //Task<List<UserDto>> UserLogin(UserModel model);
        public UserDetailsDto GetUserProfile(LogindBindingModel model, string apiConnectionAuthString);
        //public UserDetailsDto GetUserProfileAfterPasswordReset(ChangePasswordBindingModel model, string apiConnectionAuthString);
    }
}
