﻿using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using Sales.Infra.Interfaces;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Implementation
{
    public class MyPostService : IMyPostService
    {
        private readonly IMyPost _MyPostRepo;
        public MyPostService(IMyPost MyPostRepo)
        {
            _MyPostRepo = MyPostRepo;
        }

        /// <summary>
        /// Get post type
        /// </summary>
        /// <returns></returns>
        public async Task<List<PostTypeDto>> GetPostTypeList()
        {
            List<PostTypeDto> obj = new List<PostTypeDto>();
            try
            {
                obj = await _MyPostRepo.GetPostTypeList();
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        /// <summary>
        /// Get privacy type
        /// </summary>
        /// <returns></returns>
        public async Task<List<PrivacyTypeDto>> GetPrivacyTypeList()
        {
            List<PrivacyTypeDto> obj = new List<PrivacyTypeDto>();
            try
            {
                obj = await _MyPostRepo.GetPrivacyTypeList();
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<UserPostDto>> InsertUserPostData(UserPostModel model)
        {

            List<UserPostDto> obj = new List<UserPostDto>();
            try
            {
                obj = await _MyPostRepo.InsertUserPostData(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<UserPostDto>> InsertUserPostMediaData(UserPostMediaModel model)
        {

            List<UserPostDto> obj = new List<UserPostDto>();
            try
            {
                obj = await _MyPostRepo.InsertUserPostMediaData(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostDataMediaDto>> GetUserPoseMediadata(PostMediaModel model)
        {

            List<PostDataMediaDto> obj = new List<PostDataMediaDto>();
            try
            {
                obj = await _MyPostRepo.GetUserPoseMediadata(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<CompanyDto>> GetCompanyList(string CompanyCode)
        {

            List<CompanyDto> obj = new List<CompanyDto>();
            try
            {
                obj = await _MyPostRepo.GetCompanyList(CompanyCode);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<BrokerGroupDto>> GetGroupByCompany(GroupModel Model)
        {

            List<BrokerGroupDto> obj = new List<BrokerGroupDto>();
            try
            {
                obj = await _MyPostRepo.GetGroupByCompany(Model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<DivisionCompanyDto>> GetDivisionByCompany(DivisionModel Model)
        {

            List<DivisionCompanyDto> obj = new List<DivisionCompanyDto>();
            try
            {
                obj = await _MyPostRepo.GetDivisionByCompany(Model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<ZoneDto>> GetAllZone(ZoneModel model)
        {

            List<ZoneDto> obj = new List<ZoneDto>();
            try
            {
                obj = await _MyPostRepo.GetAllZone(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostLocationDto>> InsertUserPostLocation(UserPostLocation model)
        {

            List<PostLocationDto> obj = new List<PostLocationDto>();
            try
            {
                obj = await _MyPostRepo.InsertUserPostLocation(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostTagDto>> InsertUserPostTag(UserPostTag model)
        {

            List<PostTagDto> obj = new List<PostTagDto>();
            try
            {
                obj = await _MyPostRepo.InsertUserPostTag(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostTagDto>> MakePostFavourite(UserPostTag model)
        {

            List<PostTagDto> obj = new List<PostTagDto>();
            try
            {
                obj = await _MyPostRepo.MakePostFavourite(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostDataMediaDto>> GetUserFavoritePoseMediadata(PostMediaModel model)
        {

            List<PostDataMediaDto> obj = new List<PostDataMediaDto>();
            try
            {
                obj = await _MyPostRepo.GetUserFavoritePoseMediadata(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostDataMediaDto>> GetUserPoseMediadataNotification(NotificID model)
        {

            List<PostDataMediaDto> obj = new List<PostDataMediaDto>();
            try
            {
                obj = await _MyPostRepo.GetUserPoseMediadataNotification(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<NotificID>> GetUserListByZoneGroupDiv(GetUserListByZoneGroupDiv model)
        {
            List<NotificID> obj = new List<NotificID>();
            try
            {
                obj = await _MyPostRepo.GetUserListByZoneGroupDiv(model);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<NotificID>> PostComment(PostComment _postComment)
        {
            List<NotificID> obj = new List<NotificID>();
            try
            {
                obj = await _MyPostRepo.PostComment(_postComment);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<CommentCount>> CommentCountByUserId(CommentPostNo _postNo)
        {
            List<CommentCount> obj = new List<CommentCount>();
            try
            {
                obj = await _MyPostRepo.CommentCountByUserId(_postNo);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostCommenText>> CommentedText(PostCommentList __postCmt)
        {
            List<PostCommenText> obj = new List<PostCommenText>();
            try
            {
                obj = await _MyPostRepo.CommentedText(__postCmt);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostLocationDto>> ReplyComment(PostCommenText postCommenText)
        {
            List<PostLocationDto> obj = new List<PostLocationDto>();
            try
            {
                obj = await _MyPostRepo.ReplyComment(postCommenText);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<PostCommentList>> GetCommentedUser(CommentPostNo _postNo)
        {
            List<PostCommentList> obj = new List<PostCommentList>();
            try
            {
                obj = await _MyPostRepo.GetCommentedUser(_postNo);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<SaveCammentCount>> GetSaveCammentCount(PostCommentList _postCommentList)
        {
            List<SaveCammentCount> obj = new List<SaveCammentCount>();
            try
            {
                obj = await _MyPostRepo.GetSaveCammentCount(_postCommentList);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }
    }
}
