﻿using Sales.DTO.Models;
using Sales.Infra.Interfaces;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Implementation
{
    public class AdminService : IAdminService
    {
        private readonly IAdmin _AdminRepo;
        public AdminService(IAdmin MyPostRepo)
        {
            _AdminRepo = MyPostRepo;
        }

        public async Task<List<LocationFromView>> GetLocationFromView(PostTagDto _postNo)
        {
            List<LocationFromView> _locFromView = new List<LocationFromView>();
            try
            {
                _locFromView = await _AdminRepo.GetLocationFromView(_postNo);
            }
            catch (Exception ex)
            {
                _locFromView = null;
            }
            return _locFromView;
        }

        public async Task<List<LocationFromViewdetails>> GetLocationFromViewdetails(PostTagDto _postNo)
        {
            List<LocationFromViewdetails> obj = new List<LocationFromViewdetails>();
            try
            {
                obj = await _AdminRepo.GetLocationFromViewdetails(_postNo);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<GetPostFavouritesCount>> GetPostFavouritesCount(PostTagDto _postNo)
        {
            List<GetPostFavouritesCount> _postFavCount = new List<GetPostFavouritesCount>();
            try
            {
                _postFavCount = await _AdminRepo.GetPostFavouritesCount(_postNo);
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }

        public async Task<List<GetPostReachCount>> GetPostReachCount(PostTagDto _postNo)
        {
            List<GetPostReachCount> _postReachCount = new List<GetPostReachCount>();
            try
            {
                _postReachCount = await _AdminRepo.GetPostReachCount(_postNo);
            }
            catch (Exception ex)
            {
                _postReachCount = null;
            }
            return _postReachCount;
        }

        public async Task<List<PrivacyTypeDto>> GetPrivacyTypeList()
        {
            List<PrivacyTypeDto> obj = new List<PrivacyTypeDto>();
            try
            {
                obj = await _AdminRepo.GetPrivacyTypeList();
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

    }
}
