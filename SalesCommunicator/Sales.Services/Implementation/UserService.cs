﻿using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using Sales.Infra.Interfaces;
using Sales.Repositories.Implementation;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Implementation
{
    public class UserService : IUserService
    {
        //private readonly IUserRepository _UserRepo;
        //public UserService(IUserRepository UserRepo)
        //{
        //    _UserRepo = UserRepo;
        //}
        //public async Task<List<UserDto>> UserLogin(UserModel model)
        //{
        //    List<UserDto> obj = new List<UserDto>();
        //    try
        //    {
        //        obj = await _UserRepo.Login(model);
        //    }
        //    catch (Exception ex)
        //    {
        //        obj = null;
        //    }
        //    return obj;
        //}

        //private IUserRepository userRepository;
        string apiConnectionAuthString;
        public IUserRepository repObj = null;
        private UserDetailsDto appuser;
        public UserService()
        {
            //this.userRepository = userRepository;
            //apiConnectionAuthString = connectionString;
        }
        public UserDetailsDto GetUserProfile(LogindBindingModel model, string apiConnectionAuthString)
        {
            repObj = new UserRepository();
            appuser = repObj.GetUserProfile(model, apiConnectionAuthString);
            return appuser;
        }
        //public UserDetailsDto GetUserProfileAfterPasswordReset(ChangePasswordBindingModel model, string apiConnectionAuthString)
        //{
        //    repObj = new UserRepository();
        //    LogindBindingModel loginuser = new LogindBindingModel { UserName = model.UserName, Password = model.OldPassword };

        //    appuser = repObj.GetUserProfile(loginuser, apiConnectionAuthString);

        //    if (appuser != null)
        //    {
        //        bool appuserupdate = repObj.UpdateUserPassword(model, apiConnectionAuthString);
        //        if (!appuserupdate)
        //        {
        //            appuser = null;
        //        }
        //    }

        //    return appuser;
        //}
    }
}
