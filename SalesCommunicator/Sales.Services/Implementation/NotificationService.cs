﻿using Sales.DTO.Models;
using Sales.Infra.Interfaces;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Services.Implementation
{
    public class NotificationService : INotificationService
    {
        private readonly INotification notificationService;

        public NotificationService(INotification _notificationService)
        {
            notificationService = _notificationService;
        }

        public async Task<List<DeleteOutputNotfy>> DeleteNotification(DeleteNotification delete)
        {
            List<DeleteOutputNotfy> obj = new List<DeleteOutputNotfy>();
            try
            {
                obj = await notificationService.DeleteNotification(delete);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<Count>> GetCounts(GetNotification _userid)
        {
            List<Count> obj = new List<Count>();
            try
            {
                obj = await notificationService.GetCounts(_userid);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<Notification>> GetNotification(GetNotification getNotification)
        {
            List<Notification> _notfic = new List<Notification>();
            try
            {
                _notfic = await notificationService.GetNotification(getNotification);
            }
            catch (Exception ex)
            {
                _notfic = null;
            }
            return _notfic;
        }

        public async Task<List<Notification>> InsertNotification(Notification notification)
        {
            List<Notification> obj = new List<Notification>();
            try
            {
                obj = await notificationService.InsertNotification(notification);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }

        public async Task<List<UpdateNotification>> UpdateNotification(GetNotification update)
        {
            List<UpdateNotification> obj = new List<UpdateNotification>();
            try
            {
                obj = await notificationService.UpdateNotification(update);
            }
            catch (Exception ex)
            {
                obj = null;
            }
            return obj;
        }
    }
}
