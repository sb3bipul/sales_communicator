using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Sales.EfCore.DataContext;
using Sales.Infra.Interfaces;
using Sales.Repositories.Implementation;
using Sales.Services.Implementation;
using Sales.Services.Interfaces;
using Sales.Services.ModelMapper;

namespace SalesCommunicator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddDbContext<DBContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("conStr")));
            services.AddCors(c =>
            {
                c.AddPolicy("AllowAllOrigins", options => options.AllowAnyOrigin()
                                                              .AllowAnyMethod()
                                                              .AllowAnyHeader());
            });

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    //ValidIssuer = Configuration["Jwt:Issuer"],
                    //ValidAudience = Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:SecretKey"])),
                    ClockSkew = TimeSpan.Zero
                };
            });

            services.AddScoped<IMapperFactory, MapperFactory>();
            //Repositories
            //services.AddScoped<ICustomers, CustomerRepository>();

            //services

            services.AddScoped<IMyPostService, MyPostService>();
            services.AddScoped<IMyPost, MyPostRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IAdmin, AdminRepository>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<INotificationService, NotificationService>();
            services.AddScoped<INotification, NotificationRepository>();
            //services.AddScoped<ICustomerService, CustomerService>();
            //services.AddScoped<IUsers, UsersRepository>();
            //services.AddScoped<IUserService, UserService>();
            //services.AddScoped<IItem, ItemRepository>();
            //services.AddScoped<IItemService, ItemService>();
            //services.AddScoped<IOrders, OrderRepository>();
            //services.AddScoped<IOrderService, OrderService>();
            //services.AddScoped<IPriceList, PriceListRepository>();
            //services.AddScoped<IPriceListService, PriceListService>();

            //services.AddScoped<IOrderHistories, OrderHistoryRepository>();
            //services.AddScoped<IOrderHistoryServices, OrderHistoryService>();
            //services.AddScoped<IOrderStatus, OrderStatusRepository>();
            //services.AddScoped<IOrderStatusService, OrderStatusService>();
            //    services.AddControllers()
            //.AddNewtonsoftJson(options =>
            //{
            //    options.UseMemberCasing();
            //});
            #region swagger integration
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(Configuration["Swagger:version"], new OpenApiInfo
                {
                    Title = Configuration["Swagger:title"],
                    Version = Configuration["Swagger:version"],
                });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = Configuration["Swagger:description"], //"JWT Authorization header using the Bearer scheme. \r\n\r\n Enter 'Bearer' [space] and then your token in the text input below.\r\n\r\nExample: \"Bearer 12345abcdef\"",
                    Name = Configuration["Swagger:name"],
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,

                        },
                        new List<string>()
                        }
                 });
            });

            //services.AddSwaggerGen(options => options.DocumentFilter<RemoveSchemasFilter>());
            #endregion
            services.AddControllers().AddJsonOptions(JsonOptions =>
            {
                JsonOptions.JsonSerializerOptions.PropertyNamingPolicy = null;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        //{
        //    if (env.IsDevelopment())
        //    {
        //        app.UseDeveloperExceptionPage();
        //    }

        //    app.UseHttpsRedirection();

        //    app.UseRouting();

        //    app.UseAuthorization();

        //    app.UseEndpoints(endpoints =>
        //    {
        //        endpoints.MapControllers();
        //    });
        //}

        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        //{
        //    if (env.IsDevelopment())
        //    {
        //        //app.UseSwagger();
        //        //app.UseSwaggerUI(c =>
        //        //{
        //        //    c.SwaggerEndpoint(Configuration["Swagger:swaggerurl"], Configuration["Swagger:swaggertitle"]);
        //        //});
        //        app.UseDeveloperExceptionPage();
        //    }

        //    app.UseSwagger();
            
        //    app.UseSwaggerUI(c =>
        //    {
        //        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sales Communicator API");
        //        //c.RoutePrefix = string.Empty;
        //        c.DefaultModelsExpandDepth(-1);
        //    });

        //    app.UseCors(options =>
        //        options.AllowAnyOrigin()
        //        .AllowAnyMethod()
        //        .AllowAnyHeader()
        //        );
        //    app.UseHttpsRedirection();

        //    app.UseRouting();
        //    app.UseAuthentication();
        //    app.UseAuthorization();

        //    app.UseEndpoints(endpoints =>
        //    {
        //        endpoints.MapControllers();
        //    });

        //    loggerFactory.AddLog4Net();
        //}

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors("AllowAllOrigins");
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            if(env.IsDevelopment() || env.IsProduction())
            {
                app.UseSwagger();
                //app.UseSwaggerUI(options => options.SwaggerEndpoint("/swagger/v2/swagger.json", "PlaceInfo Services"));
                app.UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sales communicator API");
                    //c.RoutePrefix = string.Empty;
                    //c.DefaultModelsExpandDepth(-1);
                });
            }            
        }
    }
}
