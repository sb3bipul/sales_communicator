﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sales.DTO.Models;
using Sales.Infra.Interfaces;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesCommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class NotificationController : ControllerBase
    {
        private readonly INotificationService _notification;
        string _client = "Test2";

        public NotificationController(INotificationService notification)
        {
            _notification = notification;
        }

        [HttpGet]
        [Route("GetNotification")]
        public async Task<IActionResult> GetNotification(GetNotification getNotification)
        {
            var DomainName = "GetNotification";
            GenericResponse<List<Notification>> genericResponse = new Sales.DTO.Models.GenericResponse<List<Notification>>();
            List<Notification> _notify = await _notification.GetNotification(getNotification);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = _notify;
            if (_notify != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        [HttpPost]
        [Route("InsertNotification")]
        public async Task<IActionResult> InsertNotification(Notification notification)
        {
            var DomainName = "GetNotification";
            GenericResponse<List<Notification>> genericResponse = new Sales.DTO.Models.GenericResponse<List<Notification>>();
            List<Notification> _notify = await _notification.InsertNotification(notification);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = _notify;
            if (_notify != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("UpdateNotification")]
        public async Task<IActionResult> UpdateNotification(GetNotification updateDelete)
        {
            var DomainName = "GetNotification";
            GenericResponse<List<UpdateNotification>> genericResponse = new Sales.DTO.Models.GenericResponse<List<UpdateNotification>>();
            List<UpdateNotification> _notify = await _notification.UpdateNotification(updateDelete);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = _notify;
            if (_notify != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        [HttpPost]
        [Route("DeleteNotification")]
        public async Task<IActionResult> DeleteNotification(DeleteNotification delete)
        {
            var DomainName = "GetNotification";
            GenericResponse<List<DeleteOutputNotfy>> genericResponse = new Sales.DTO.Models.GenericResponse<List<DeleteOutputNotfy>>();
            List<DeleteOutputNotfy> _notify = await _notification.DeleteNotification(delete);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = _notify;
            if (_notify != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("getNotificationCount")]
        public async Task<IActionResult> getNotificationCount(GetNotification _get)
        {
            var DomainName = "GetNotification";
            GenericResponse<List<Count>> genericResponse = new Sales.DTO.Models.GenericResponse<List<Count>>();
            List<Count> _notify = await _notification.GetCounts(_get);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = _notify;
            if (_notify != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        //============Post data in Notification===========//    
    }
}
