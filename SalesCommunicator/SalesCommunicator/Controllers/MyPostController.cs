﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using Sales.Services.Interfaces;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SalesCommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class MyPostController : ControllerBase
    {
        private readonly IMyPostService _MyPostService;
        string _client = "Test2";
        public MyPostController(IMyPostService MyPostService)
        {
            _MyPostService = MyPostService;
        }

        /// <summary>
        /// Get post type list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPostTypeList")]
        public async Task<IActionResult> GetPostTypeList()
        {
            var DomainName = "GetPostTypeList";
            GenericResponse<List<PostTypeDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostTypeDto>>();
            List<PostTypeDto> objPosttypeList = await _MyPostService.GetPostTypeList();
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPosttypeList;
            if (objPosttypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Get Privacy list
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPrivacyTypeList")]
        public async Task<IActionResult> GetPrivacyTypeList()
        {
            var DomainName = "GetPrivacyTypeList";
            GenericResponse<List<PrivacyTypeDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PrivacyTypeDto>>();
            List<PrivacyTypeDto> objPrivacytypeList = await _MyPostService.GetPrivacyTypeList();
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Insert User Post Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("InsertUserPostData")]
        public async Task<IActionResult> InsertUserPostData(UserPostModel model)
        {
            var DomainName = "InsertUserPostData";
            GenericResponse<List<UserPostDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<UserPostDto>>();
            List<UserPostDto> objUserPost = await _MyPostService.InsertUserPostData(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPost;
            if (objUserPost != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Insert User Post Media Data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("InsertUserPostMediaData")]
        public async Task<IActionResult> InsertUserPostMediaData(UserPostMediaModel model)
        {
            var DomainName = "InsertUserPostMediaData";
            GenericResponse<List<UserPostDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<UserPostDto>>();
            List<UserPostDto> objUserPostMedia = await _MyPostService.InsertUserPostMediaData(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostMedia;
            if (objUserPostMedia != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }


        /// <summary>
        /// Get User Pose Media data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetUserPoseMediadata")]
        public async Task<IActionResult> GetUserPoseMediadata(PostMediaModel model)
        {
            var DomainName = "GetUserPoseMediadata";
            GenericResponse<List<PostDataMediaDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostDataMediaDto>>();
            List<PostDataMediaDto> objOrderStatusList = await _MyPostService.GetUserPoseMediadata(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objOrderStatusList;
            if (objOrderStatusList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Get all company List
        /// </summary>
        /// <param name="CompanyCode"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetCompanyList")]
        public async Task<IActionResult> GetCompanyList(string CompanyCode)
        {
            var DomainName = "GetCompanyList";
            GenericResponse<List<CompanyDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<CompanyDto>>();
            List<CompanyDto> objCompany = await _MyPostService.GetCompanyList(CompanyCode);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objCompany;
            if (objCompany != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }


        /// <summary>
        /// Get group list by company
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetGroupByCompany")]
        public async Task<IActionResult> GetGroupByCompany(GroupModel Model)
        {
            var DomainName = "GetGroupByCompany";
            GenericResponse<List<BrokerGroupDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<BrokerGroupDto>>();
            List<BrokerGroupDto> objGroup = await _MyPostService.GetGroupByCompany(Model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objGroup;
            if (objGroup != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Get broker division by company
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDivisionByCompany")]
        public async Task<IActionResult> GetDivisionByCompany(DivisionModel Model)
        {
            var DomainName = "GetDivisionByCompany";
            GenericResponse<List<DivisionCompanyDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<DivisionCompanyDto>>();
            List<DivisionCompanyDto> objDivision = await _MyPostService.GetDivisionByCompany(Model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objDivision;
            if (objDivision != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Get all zone by company
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetAllZone")]
        public async Task<IActionResult> GetAllZone(ZoneModel model)
        {
            var DomainName = "GetAllZone";
            GenericResponse<List<ZoneDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<ZoneDto>>();
            List<ZoneDto> objZone = await _MyPostService.GetAllZone(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objZone;
            if (objZone != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Insert user post location
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("InsertUserPostLocation")]
        public async Task<IActionResult> InsertUserPostLocation(UserPostLocation model)
        {
            var DomainName = "InsertUserPostLocation";
            GenericResponse<List<PostLocationDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostLocationDto>>();
            List<PostLocationDto> objUserPostLoc = await _MyPostService.InsertUserPostLocation(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostLoc;
            if (objUserPostLoc != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }


        /// <summary>
        /// Insert user post tag
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUserPostTag")]
        public async Task<IActionResult> InsertUserPostTag(UserPostTag model)
        {
            var DomainName = "InsertUserPostTag";
            GenericResponse<List<PostTagDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostTagDto>>();
            List<PostTagDto> objUserPostTag = await _MyPostService.InsertUserPostTag(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("MakePostFavourite")]
        public async Task<IActionResult> MakePostFavourite(UserPostTag model)
        {
            var DomainName = "MakePostFavourite";
            GenericResponse<List<PostTagDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostTagDto>>();
            List<PostTagDto> objUserPostTag = await _MyPostService.MakePostFavourite(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetUserFavoritePoseMediadata")]
        public async Task<IActionResult> GetUserFavoritePoseMediadata(PostMediaModel model)
        {
            var DomainName = "GetUserFavoritePoseMediadata";
            GenericResponse<List<PostDataMediaDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostDataMediaDto>>();
            List<PostDataMediaDto> objOrderStatusList = await _MyPostService.GetUserFavoritePoseMediadata(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objOrderStatusList;
            if (objOrderStatusList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetUserPoseMediadataNotification")]
        public async Task<IActionResult> GetUserPoseMediadataNotification(NotificID model)
        {
            var DomainName = "GetUserPoseMediadataNotification";
            GenericResponse<List<PostDataMediaDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostDataMediaDto>>();
            List<PostDataMediaDto> objOrderStatusList = await _MyPostService.GetUserPoseMediadataNotification(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objOrderStatusList;
            if (objOrderStatusList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Get User List by Zone,Group and Div
        ///</summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("GetUserListByZoneGroupDiv")]
        public async Task<IActionResult> GetUserListByZoneGroupDiv(GetUserListByZoneGroupDiv model)
        {
            var DomainName = "GetUserListByZoneGroupDiv";
            GenericResponse<List<NotificID>> genericResponse = new Sales.DTO.Models.GenericResponse<List<NotificID>>();
            List<NotificID> objUserPostTag = await _MyPostService.GetUserListByZoneGroupDiv(model);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Add comment against the post
        ///</summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("Postcomment")]
        public async Task<IActionResult> PostComment(PostComment _postComment)
        {
            var DomainName = "AddComment";
            GenericResponse<List<NotificID>> genericResponse = new Sales.DTO.Models.GenericResponse<List<NotificID>>();
            List<NotificID> objUserPostTag = await _MyPostService.PostComment(_postComment);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Count comment by loggedIn User
        ///</summary>
        [HttpGet]
        [Route("getcommentcount")]
        public async Task<IActionResult> GetCommentCountByUserId(CommentPostNo _postNo)
        {
            var DomainName = "Commentcount";
            GenericResponse<List<CommentCount>> genericResponse = new Sales.DTO.Models.GenericResponse<List<CommentCount>>();
            List<CommentCount> objUserPostTag = await _MyPostService.CommentCountByUserId(_postNo);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Commented list by user in particular post
        ///</summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("PostcommentList")]
        public async Task<IActionResult> GetCommentedText(PostCommentList __postCmt)
        {
            var DomainName = "CommentedList";
            GenericResponse<List<PostCommenText>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostCommenText>>();
            List<PostCommenText> objUserPostTag = await _MyPostService.CommentedText(__postCmt);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Reply Comment
        ///</summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("replycomment")]
        public async Task<IActionResult> AdminReplyComment(PostCommenText postCommenText)
        {
            var DomainName = "ReplyComment";
            GenericResponse<List<PostLocationDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostLocationDto>>();
            List<PostLocationDto> objUserPostTag = await _MyPostService.ReplyComment(postCommenText);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objUserPostTag;
            if (objUserPostTag != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        /// <summary>
        /// Get Commeted User list
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [Route("Commenteduserlist")]
        public async Task<IActionResult> GetCommentedUser(CommentPostNo _postNo)
        {
            var DomainName = "GetCommentedUserList";
            GenericResponse<List<PostCommentList>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PostCommentList>>();
            List<PostCommentList> objPrivacytypeList = await _MyPostService.GetCommentedUser(_postNo);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }

        ///<summary>
        ///Get comment count after save comment
        ///</summary>
        [HttpPost]
        [AllowAnonymous]
        [Route("Getsavecommentcount")]
        public async Task<IActionResult> Getsavecommentcount(PostCommentList _postCommentList)
        {
            var DomainName = "Getsavecommentcount";
            GenericResponse<List<SaveCammentCount>> genericResponse = new Sales.DTO.Models.GenericResponse<List<SaveCammentCount>>();
            List<SaveCammentCount> objPrivacytypeList = await _MyPostService.GetSaveCammentCount(_postCommentList);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);
        }
    }
}
