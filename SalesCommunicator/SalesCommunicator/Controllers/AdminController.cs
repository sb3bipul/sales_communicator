﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Sales.DTO.Models;
using Sales.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SalesCommunicator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [EnableCors("AllowAllOrigins")]
    public class AdminController : ControllerBase
    {

        private readonly IAdminService _AdminService;
        string _client = "Test2";
        public AdminController(IAdminService AdminServic)
        {
            _AdminService = AdminServic;
        }

        [HttpGet]
        [Route("GetPrivacyTypeList")]
        public async Task<IActionResult> GetPrivacyTypeList()
        {
            var DomainName = "GetPrivacyTypeList";
            GenericResponse<List<PrivacyTypeDto>> genericResponse = new Sales.DTO.Models.GenericResponse<List<PrivacyTypeDto>>();
            List<PrivacyTypeDto> objPrivacytypeList = await _AdminService.GetPrivacyTypeList();
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);

        }

        [HttpPost]
        [Route("LocationFromViewdetails")]
        public async Task<IActionResult> LocationFromViewdetails(PostTagDto _postt)
        {
            var DomainName = "LocationFromViewdetails";
            GenericResponse<List<LocationFromViewdetails>> genericResponse = new Sales.DTO.Models.GenericResponse<List<LocationFromViewdetails>>();
            List<LocationFromViewdetails> objPrivacytypeList = await _AdminService.GetLocationFromViewdetails(_postt);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);

        }

        [HttpPost]
        [Route("LocationFromView")]
        public async Task<IActionResult> LocationFromView(PostTagDto _postt)
        {
            var DomainName = "LocationFromView";
            GenericResponse<List<LocationFromView>> genericResponse = new Sales.DTO.Models.GenericResponse<List<LocationFromView>>();
            List<LocationFromView> objPrivacytypeList = await _AdminService.GetLocationFromView(_postt);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);

        }


        [HttpGet]
        [Route("GetPostFavouritesCount")]
        public async Task<IActionResult> GetPostFavouritesCount(PostTagDto _postt)
        {
            var DomainName = "GetPostFavouritesCount";
            GenericResponse<List<GetPostFavouritesCount>> genericResponse = new Sales.DTO.Models.GenericResponse<List<GetPostFavouritesCount>>();
            List<GetPostFavouritesCount> objPrivacytypeList = await _AdminService.GetPostFavouritesCount(_postt);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);

        }

        [HttpGet]
        [Route("GetPostReachCount")]
        public async Task<IActionResult> GetPostReachCount(PostTagDto _postt)
        {
            var DomainName = "PostReachCount";
            GenericResponse<List<GetPostReachCount>> genericResponse = new Sales.DTO.Models.GenericResponse<List<GetPostReachCount>>();
            List<GetPostReachCount> objPrivacytypeList = await _AdminService.GetPostReachCount(_postt);
            genericResponse.Domain = DomainName;
            genericResponse.Event = DomainName;
            genericResponse.ClientData = _client;
            genericResponse.Payload = objPrivacytypeList;
            if (objPrivacytypeList != null)
            {
                genericResponse.Status = true;
            }
            else
            {
                genericResponse.Status = false;
            }
            return Ok(genericResponse);

        }
    }
}
