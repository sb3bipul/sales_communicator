﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Sales.EfCore.Entity
{
    public partial class tblt_UserPostData
    {
        [Key]
        [Required]
        public int POSTID { get; set; }
        public string POSTNO { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime? CREATEDON { get; set; }
        public string PRIVACYTYPEID { get; set; }
        public string POSTTYPEID { get; set; }
        public string COMMENTS { get; set; }
        public DateTime FROMDATE { get; set; }
        public DateTime TODATE { get; set; }
        public Boolean? ISACTIVE { get; set; }
        public Boolean? ISDELETED { get; set; }
        public string UPDATEDBY { get; set; }
        public DateTime? UPDATEDON { get; set; }
        public string Header { get; set; }
        public string CompanyId { get; set; }
        public string DivisionId { get; set; }
        public string GroupId { get; set; }
    }
}
