﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.EfCore.Entity
{
    public partial class tblt_PostMediaDetails
    {
        public int ID { get; set; }
        public string POSTNO { get; set; }
        public string MEDIAFILENAME { get; set; }
        public string FILEURL { get; set; }
        public Boolean? ISACTIVE { get; set; }
        public Boolean? ISDELETED { get; set; }
    }
}
