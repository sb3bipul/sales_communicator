﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.EfCore.Entity
{
    public partial class tblm_PrivacyType
    {
        public int ID { get; set; }
        public string PRIVACYTYPE { get; set; }
        public Boolean? ISDELETED { get; set; }
        public Boolean? ISACTIVE { get; set; }
    }
}
