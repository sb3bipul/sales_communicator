﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Sales.DTO.Models;
using Sales.EfCore.Entity;
using Sales.DTO.Models.RequestModel;

namespace Sales.EfCore.DataContext
{
    public partial class DBContext : DbContext
    {
        public DBContext()
        {
        }
        public DBContext(DbContextOptions<DBContext> options) : base(options)
        {
        }

        public virtual DbSet<tblm_PostType> TbmPostType { get; set; }
        public virtual DbSet<tblm_PrivacyType> TbmPrivacyType { get; set; }
        public virtual DbSet<tblt_UserPostData> TblUserPost { get; set; }
        public virtual DbSet<tblt_PostMediaDetails> TblUserPostMedia { get; set; }
        public virtual DbSet<LocationFromViewdetails> LocationFromViewdetails { get; set; }
        public virtual DbSet<LocationFromView> LocationFromView { get; set; }
        public virtual DbSet<GetPostFavouritesCount> GetPostFavouritesCount { get; set; }
        public virtual DbSet<GetPostReachCount> GetPostReachCount { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<GetNotification> GetNotification { get; set; }
        public virtual DbSet<UpdateNotification> UpdateNotification { get; set; }
        public virtual DbSet<DeleteNotification> DeleteNotification { get; set; }
        public virtual DbSet<DeleteOutputNotfy> DeleteOutputNotfy { get; set; }
        public virtual DbSet<UpdateOutputNotfy> UpdateOutputNotfy { get; set; }
        public virtual DbSet<Count> Count { get; set; }
        public virtual DbSet<NotificID> Notify { get; set; }
        public virtual DbSet<CommentCount> CommentCount { get; set; }
        public virtual DbSet<PostCommenText> PostCommenText { get; set; }
        public virtual DbSet<PostCommentList> PostCommentList { get; set; }
        public virtual DbSet<SaveCammentCount> SaveCammentCount { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                ///optionsBuilder.UseSqlServer("Data Source=SB3DEV-09;Initial Catalog=SalesCommunicator;Persist Security Info=True;User ID=sa;pwd=Tr!v3n!");
                optionsBuilder.UseSqlServer("Data Source=3.221.58.176;Initial Catalog=SalesCommunicator;Persist Security Info=True;User ID=sa;pwd=$b3sql@dmin");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<tblt_UserPostData>().HasNoKey();
            modelBuilder.Entity<tblt_UserPostData>(entity =>
            {
                entity.ToTable("tblt_UserPostData");
                entity.Property(e => e.POSTID).HasColumnName("POSTID");

                entity.Property(e => e.POSTNO).HasMaxLength(100).IsUnicode(false);
                entity.Property(e => e.CREATEDBY).HasMaxLength(500).IsUnicode(false);
                entity.Property(e => e.CREATEDON).HasColumnType("datetime");
                entity.Property(e => e.PRIVACYTYPEID).HasMaxLength(10).IsUnicode(false);
                entity.Property(e => e.POSTTYPEID).HasMaxLength(10).IsUnicode(false);
                entity.Property(e => e.COMMENTS).HasMaxLength(500).IsUnicode(false);
                entity.Property(e => e.FROMDATE).HasColumnType("datetime");
                entity.Property(e => e.TODATE).HasColumnType("datetime");
                entity.Property(e => e.UPDATEDBY).HasMaxLength(500).IsUnicode(false);
                entity.Property(e => e.UPDATEDON).HasColumnType("datetime");
                entity.Property(e => e.Header).HasMaxLength(250).IsUnicode(false);
                entity.Property(e => e.CompanyId).HasMaxLength(10).IsUnicode(false);
                entity.Property(e => e.DivisionId).HasMaxLength(100).IsUnicode(false);
                entity.Property(e => e.GroupId).HasMaxLength(100).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
            modelBuilder.Entity<PostTypeDto>().HasNoKey();
            modelBuilder.Entity<PrivacyTypeDto>().HasNoKey();
            modelBuilder.Entity<UserPostDto>().HasNoKey();
            modelBuilder.Entity<UserDto>().HasNoKey();
            modelBuilder.Entity<PostDataDto>().HasNoKey();
            modelBuilder.Entity<CompanyDto>().HasNoKey();
            modelBuilder.Entity<BrokerGroupDto>().HasNoKey();
            modelBuilder.Entity<DivisionCompanyDto>().HasNoKey();
            modelBuilder.Entity<ZoneDto>().HasNoKey();
            modelBuilder.Entity<PostLocationDto>().HasNoKey();
            modelBuilder.Entity<PostTagDto>().HasNoKey();
            modelBuilder.Entity<LocationFromViewdetails>().HasNoKey();
            modelBuilder.Entity<GetPostFavouritesCount>().HasNoKey();
            modelBuilder.Entity<LocationFromView>().HasNoKey();
            modelBuilder.Entity<GetPostReachCount>().HasNoKey();
            modelBuilder.Entity<GetNotification>().HasNoKey();
            modelBuilder.Entity<UpdateNotification>().HasNoKey();
            modelBuilder.Entity<DeleteNotification>().HasNoKey();
            modelBuilder.Entity<DeleteOutputNotfy>().HasNoKey();
            modelBuilder.Entity<UpdateOutputNotfy>().HasNoKey();
            modelBuilder.Entity<Notification>().HasNoKey();
            modelBuilder.Entity<Count>().HasNoKey();
            modelBuilder.Entity<NotificID>().HasNoKey();
            modelBuilder.Entity<CommentCount>().HasNoKey();
            modelBuilder.Entity<PostCommenText>().HasNoKey();
            modelBuilder.Entity<PostCommentList>().HasNoKey();
            modelBuilder.Entity<SaveCammentCount>().HasNoKey();
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
