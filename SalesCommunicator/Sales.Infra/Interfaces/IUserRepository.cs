﻿using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infra.Interfaces
{
    public interface IUserRepository
    {
        //Task<List<UserDto>> Login(UserModel model);
        public UserDetailsDto GetUserProfileByUserLoginId(string userLoginId, string apiConnectionAuthString);
        public UserDetailsDto GetUserProfile(LogindBindingModel model, string apiConnectionAuthString);
        //public bool UpdateUserPassword(ChangePasswordBindingModel model, string apiConnectionAuthString);
    }
}
