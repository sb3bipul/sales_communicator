﻿using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infra.Interfaces
{
    public interface IMyPost
    {
        Task<List<PostTypeDto>> GetPostTypeList();
        Task<List<PrivacyTypeDto>> GetPrivacyTypeList();
        Task<List<UserPostDto>> InsertUserPostData(UserPostModel model);
        Task<List<UserPostDto>> InsertUserPostMediaData(UserPostMediaModel model);
        Task<List<PostDataMediaDto>> GetUserPoseMediadata(PostMediaModel model);
        Task<List<CompanyDto>> GetCompanyList(string CompanyCode);
        Task<List<BrokerGroupDto>> GetGroupByCompany(GroupModel Model);
        Task<List<DivisionCompanyDto>> GetDivisionByCompany(DivisionModel Model);
        Task<List<ZoneDto>> GetAllZone(ZoneModel model);
        Task<List<PostLocationDto>> InsertUserPostLocation(UserPostLocation model);
        Task<List<PostTagDto>> InsertUserPostTag(UserPostTag model);
        Task<List<PostTagDto>> MakePostFavourite(UserPostTag model);
        Task<List<PostDataMediaDto>> GetUserFavoritePoseMediadata(PostMediaModel model);

        Task<List<PostDataMediaDto>> GetUserPoseMediadataNotification(NotificID model);
        Task<List<NotificID>> GetUserListByZoneGroupDiv(GetUserListByZoneGroupDiv model);//Added on 26/11/2021
        Task<List<NotificID>> PostComment(PostComment _postComment);
        Task<List<CommentCount>> CommentCountByUserId(CommentPostNo _postNo);
        Task<List<PostCommenText>> CommentedText(PostCommentList __postCmt);
        Task<List<PostLocationDto>> ReplyComment(PostCommenText postCommenText);
        Task<List<PostCommentList>> GetCommentedUser(CommentPostNo _postNo);
        Task<List<SaveCammentCount>> GetSaveCammentCount(PostCommentList _postCommentList);
    }
}
