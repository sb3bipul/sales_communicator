﻿using Sales.DTO.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Infra.Interfaces
{
    public interface IAdmin
    {
        Task<List<PrivacyTypeDto>> GetPrivacyTypeList();
        Task<List<LocationFromViewdetails>> GetLocationFromViewdetails(PostTagDto _postNo);
        Task<List<LocationFromView>> GetLocationFromView(PostTagDto _postNo);
        Task<List<GetPostFavouritesCount>> GetPostFavouritesCount(PostTagDto _postNo);
        Task<List<GetPostReachCount>> GetPostReachCount(PostTagDto _postNo);
    }
}
