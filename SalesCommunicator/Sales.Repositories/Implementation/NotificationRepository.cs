﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Sales.DTO.Models;
using Sales.EfCore.DataContext;
using Sales.EfCore.Entity;
using Sales.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Repositories.Implementation
{
    public class NotificationRepository:BaseRepository<tblm_PostType>, INotification
    {
        IConfiguration configuration;
        public NotificationRepository(DBContext context, IConfiguration configuration):base(context)
        {
            context.Database.SetCommandTimeout(1);
            this.configuration = configuration;
        }

        public async Task<List<Notification>> DeleteNotification(int UserID, int NotificationID)
        {
            List<Notification> _postFavCount = new List<Notification>();
            try
            {
                SqlParameter User_ID = new SqlParameter("@UserID", UserID);
                SqlParameter Notification_ID = new SqlParameter("@NotificationID", NotificationID);
                SqlParameter Flag = new SqlParameter("@Flag", 3);

                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<Notification>().FromSqlRaw("SProc_Notification_InsertUpdate  @UserID, @NotificationID,@Flag", Notification_ID, User_ID,Flag).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }
        public async Task<List<DeleteOutputNotfy>> DeleteNotification(DeleteNotification delete)
        {
            List<DeleteOutputNotfy> _postFavCount = new List<DeleteOutputNotfy>();
            try
            {
                SqlParameter User_ID = new SqlParameter("@UserID", delete.UserId);
                SqlParameter Notification_ID = new SqlParameter("@NotificationID", delete.NotificationID);

                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<DeleteOutputNotfy>().FromSqlRaw("SProc_DeletetNotificationList  @UserID,@NotificationID", User_ID, Notification_ID).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }

        public async Task<List<Notification>> GetNotification(GetNotification UserID)
        {
            List<Notification> _postFavCount = new List<Notification>();
            try
            {
                SqlParameter User_ID = new SqlParameter("@UserID",UserID.UserId);

                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<Notification>().FromSqlRaw("SProc_GetNotificationList  @UserID", User_ID).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }

        public async Task<List<Notification>> InsertNotification(Notification notification)
        {
            List<Notification> SubmitList = new List<Notification>();
            try
            {
                SqlParameter NotificationID = new SqlParameter("@NotificationID", notification.NotificationID);
                SqlParameter PostNo = new SqlParameter("@PostNo", notification.PostNo);
                SqlParameter UserId = new SqlParameter("@UserId", notification.UserId);
                SqlParameter NotificationDate = new SqlParameter("@NotificationDate", notification.NotificationDate);
                SqlParameter NotificationStatus = new SqlParameter("@NotificationStatus", notification.NotificationStatus);
                SqlParameter NotificationText = new SqlParameter("@NotificationText", notification.NotificationText);
                
                
                DBContext _context = new DBContext();
                SubmitList = await
                _context.Set<Notification>().FromSqlRaw("SProc_Notification_InsertUpdate  @NotificationID,@PostNo,@UserId,@NotificationDate,@NotificationStatus,@NotificationText",
                NotificationID, PostNo, UserId, NotificationDate, NotificationStatus, NotificationText).ToListAsync();
            }
            catch (Exception ex)
            {
                SubmitList = null;
            }
            return SubmitList;
        }

        public async Task<List<UpdateNotification>> UpdateNotification(GetNotification update)
        {
            List<UpdateNotification> _postFavCount = new List<UpdateNotification>();
            try
            {
                SqlParameter userName = new SqlParameter("@UserName", update.UserId);
                //SqlParameter Notification_ID = new SqlParameter("@NotificationID", update.NotificationID);
                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<UpdateNotification>().FromSqlRaw("SProc_UpdateNotificationList  @UserName", userName).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }


        

        public async Task<List<Count>> GetCounts(GetNotification _count)
        {
            List<Count> _postFavCount = new List<Count>();
            try
            {
                SqlParameter userName = new SqlParameter("@userName", _count.UserId);

                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<Count>().FromSqlRaw("SProc_GetCountNotification  @userName", userName).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }
    }
}
