﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Sales.DTO.Models;
using Sales.EfCore.DataContext;
using Sales.EfCore.Entity;
using Sales.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sales.Repositories.Implementation
{
    public class AdminRepository : BaseRepository<tblm_PostType>, IAdmin
    {
        IConfiguration configuration;
        public AdminRepository(DBContext context, IConfiguration configuration) : base(context)
        {
            context.Database.SetCommandTimeout(1);
            this.configuration = configuration;
        }

        public async Task<List<LocationFromView>> GetLocationFromView(PostTagDto _postNo)
        {
            List<LocationFromView> _locFromViews = new List<LocationFromView>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(_postNo.POSTNO) ? DBNull.Value : (object)_postNo.POSTNO);

                DBContext _context = new DBContext();
                _locFromViews = await
                _context.Set<LocationFromView>().FromSqlRaw("SProc_LocationFromView  @PostNo", PostNo).ToListAsync();
            }
            catch (Exception ex)
            {
                _locFromViews = null;
            }
            return _locFromViews;
        }

        public async Task<List<LocationFromViewdetails>> GetLocationFromViewdetails(PostTagDto _postNo)
        {
            List<LocationFromViewdetails> _loc = new List<LocationFromViewdetails>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(_postNo.POSTNO) ? DBNull.Value : (object)_postNo.POSTNO);

                DBContext _context = new DBContext();
                _loc = await
                _context.Set<LocationFromViewdetails>().FromSqlRaw("SProc_LocationFromViewdetails  @PostNo", PostNo).ToListAsync();
            }
            catch (Exception ex)
            {
                _loc = null;
            }
            return _loc;
        }

        
        public async Task<List<GetPostFavouritesCount>> GetPostFavouritesCount(PostTagDto _postNo)
        {
            List<GetPostFavouritesCount> _postFavCount = new List<GetPostFavouritesCount>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(_postNo.POSTNO) ? DBNull.Value : (object)_postNo.POSTNO);

                DBContext _context = new DBContext();
                _postFavCount = await
                _context.Set<GetPostFavouritesCount>().FromSqlRaw("SProc_PostFavouritesCount  @PostNo", PostNo).ToListAsync();
            }
            catch (Exception ex)
            {
                _postFavCount = null;
            }
            return _postFavCount;
        }

        public async Task<List<GetPostReachCount>> GetPostReachCount(PostTagDto _postNo)
        {
            List<GetPostReachCount> _postReachCount = new List<GetPostReachCount>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(_postNo.POSTNO) ? DBNull.Value : (object)_postNo.POSTNO);

                DBContext _context = new DBContext();
                _postReachCount = await
                _context.Set<GetPostReachCount>().FromSqlRaw("SProc_PostReachCount  @PostNo", PostNo).ToListAsync();
            }
            catch (Exception ex)
            {
                _postReachCount = null;
            }
            return _postReachCount;
        }

        public async Task<List<PrivacyTypeDto>> GetPrivacyTypeList()
        {
            List<PrivacyTypeDto> privacytype = new List<PrivacyTypeDto>();
            try
            {
                DBContext _context = new DBContext();
                privacytype = await
                _context.Set<PrivacyTypeDto>().FromSqlRaw("SProc_GetPrivacyType").ToListAsync();
            }
            catch (Exception ex)
            {
                privacytype = null;
            }
            return privacytype;
        }
    }
}
