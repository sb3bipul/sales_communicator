﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.Data.SqlClient;
using Sales.Infra.Interfaces;

namespace Sales.Repositories.Implementation
{
    public class DataAccessRepository: IBaseDataAccessRepository
    {
        private string strConnection = string.Empty;

        public DataAccessRepository(string apiConnectionAuthString)
        {
            //strConnection = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString;
            strConnection = apiConnectionAuthString;
        }
        public DataAccessRepository(Boolean isRemote, string apiConnectionAuthString)
        {
            //strConnection = ConfigurationManager.ConnectionStrings["conStrDB2"].ConnectionString;
            strConnection = apiConnectionAuthString;
        }

        public void ExecuteQuery(String strSQL)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strSQL;
                        cnn.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public void ExecuteQuery(String strSPName, ref SqlParameter[] param)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }

        public SqlDataReader GetDataReader(string StrSQL)
        {
            SqlDataReader objDataReader;
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = StrSQL;
                        cnn.Open();
                        objDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection);
                        return objDataReader;
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
        }
        public DataTable GetDataTable(string strSQL)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataTable Dt = new DataTable();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strSQL;
                        cnn.Open();
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(Dt);
                        return Dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }

        public DataTable GetDataTable(string strSPName, SqlParameter[] param)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataTable Dt = new DataTable();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(Dt);
                        return Dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }

        public DataSet GetDataSet(string strSQL)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataSet objDs = new DataSet();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strSQL;
                        cnn.Open();
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(objDs);
                        return objDs;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }

        public DataSet GetDataSet(string strSPName, SqlParameter[] param)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataSet objDs = new DataSet();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(objDs);
                        return objDs;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }

        public DataSet GetDataSet(string strSPName, Boolean isSP)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataSet objDs = new DataSet();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(objDs);
                        return objDs;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }


        public string GetScalarValue(string strSQL)
        {
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = strSQL;
                        cnn.Open();
                        return Convert.ToString(cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDataTable(string strSPName, Boolean isSP)
        {
            SqlDataAdapter objAdpt = new SqlDataAdapter();
            DataTable Dt = new DataTable();
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        objAdpt.SelectCommand = cmd;
                        objAdpt.Fill(Dt);
                        return Dt;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                objAdpt.Dispose();
            }
        }

        public string ExecuteOutParamProc(String strSPName, SqlParameter[] param)
        {
            string strMessage = "";
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }

                        cmd.Parameters.Add("@Message", SqlDbType.Char, 500);
                        cmd.Parameters["@Message"].Direction = ParameterDirection.Output;

                        int i = cmd.ExecuteNonQuery();
                        strMessage = Convert.ToString(cmd.Parameters["@Message"].Value);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return strMessage;
        }

        public string[] ExecuteOutParamProc(String strSPName, SqlParameter[] param, SqlParameter[] outparam)
        {
            string[] outputParamValues = new string[outparam.Length];
            try
            {
                using (SqlConnection cnn = new SqlConnection(strConnection))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.Connection = cnn;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = strSPName;
                        cnn.Open();
                        foreach (SqlParameter p in param)
                        {
                            cmd.Parameters.Add(p);
                        }

                        foreach (SqlParameter op in outparam)
                        {
                            op.Direction = ParameterDirection.Output;
                            cmd.Parameters.Add(op);
                        }

                        int i = cmd.ExecuteNonQuery();

                        for (int iop = 0; iop < outparam.Length; iop++)
                        {
                            outputParamValues[iop] = Convert.ToString(cmd.Parameters[iop].Value);
                        }

                    }
                }
            }
            catch (Exception Ex)
            {
                throw Ex;
            }
            return outputParamValues;
        }
    }
}
