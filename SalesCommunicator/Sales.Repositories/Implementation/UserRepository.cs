﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using Sales.EfCore.DataContext;
using Sales.EfCore.Entity;
using Sales.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.AspNetCore.Http;
using System.Web;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Sales.Repositories.Helper;
using System.Data;

namespace Sales.Repositories.Implementation
{
    public class UserRepository : IUserRepository
    {
        //HttpClient client = null;
        //private readonly IConfiguration _Configuration;
        //public UserRepository(DBContext context, IConfiguration Configuration) : base(context)
        //{
        //    _Configuration = Configuration;
        //}
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        DBConnection dbConnectionObj = null;
        SqlCommand com = null;
        SqlDataAdapter da = null;
        DataTable dt = null;
        private SqlConnection _con;

        #region New implementation
        public UserDetailsDto GetUserProfileByUserLoginId(string userLoginId, string apiConnectionAuthString)
        {
            UserDetailsDto userdetail = null;
            try
            {
                userdetail = new UserDetailsDto();
                dbConnectionObj = new DBConnection(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.GetUserMyProfile, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", userLoginId);
                com.Parameters.AddWithValue("@CompanyID", 2);
                da = new SqlDataAdapter(com);
                dt = new DataTable();

                if (_con.State != ConnectionState.Open)
                {
                    _con.Close();
                    _con.Open();
                }
                da.Fill(dt);
                if (_con.State == ConnectionState.Open)
                {
                    _con.Close();
                }
                //dbConnectionObj.ConnectionClose();

                foreach (DataRow dr in dt.Rows)
                {
                    userdetail.UserName = dr["UserName"].ToString();
                    userdetail.UserFullName = Convert.ToString(dr["UserFullName"]);
                    userdetail.Email = Convert.ToString(dr["Email"].ToString());
                    userdetail.PhoneNumber = dr["PhoneNumber"].ToString();
                    userdetail.Domain = dr["Domain"].ToString();
                    userdetail.Role = dr["Role"].ToString();
                    userdetail.RoleId = dr["RoleId"].ToString();
                    userdetail.CompanyId = dr["CompanyId"].ToString();
                    userdetail.CompanyName = dr["CompanyName"].ToString();
                    userdetail.UserType = Convert.ToString(dr["UserType"].ToString());
                    userdetail.BrokerPicName = dr["BrokerPicName"].ToString();
                    userdetail.IsNewReleaseFlag = dr["IsNewReleaseFlag"].ToString();
                    userdetail.LoginCompanyId = dr["LoginCompanyId"].ToString();
                    userdetail.BrokerDivisionCode = Convert.ToString(dr["BrokerDivisionCode"].ToString());
                    userdetail.DeliveryCharge = Convert.ToString(dr["DeliveryCharge"].ToString());
                    userdetail.MinimumDeliveryChargeAmount = dr["DefaultLanguage"].ToString();
                    userdetail.IsCreatePost = Convert.ToBoolean(dr["IsCreatePost"]);
                }

                log.Debug("Debug logging: GetUserProfileByUserLoginId");

                return userdetail;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                dbConnectionObj = null;
                com = null;
                da = null;
                dt = null;
            }

            return userdetail;
        }

        public UserDetailsDto GetUserProfileold(LogindBindingModel model, string apiConnectionAuthString)
        {
            UserDetailsDto userObj = null;
            try
            {
                userObj = new UserDetailsDto();
                dbConnectionObj = new DBConnection(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.GetUserMyProfile, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@userId", model.UserName);
                com.Parameters.AddWithValue("@CompanyID", 2);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserName = dr["UserName"].ToString();
                    userObj.UserFullName = Convert.ToString(dr["UserFullName"]);
                    userObj.Email = Convert.ToString(dr["Email"].ToString());
                    userObj.PhoneNumber = dr["PhoneNumber"].ToString();
                    userObj.Domain = dr["Domain"].ToString();
                    userObj.Role = dr["Role"].ToString();
                    userObj.RoleId = dr["RoleId"].ToString();
                    userObj.CompanyId = dr["CompanyId"].ToString();
                    userObj.CompanyName = dr["CompanyName"].ToString();
                    userObj.UserType = Convert.ToString(dr["UserType"].ToString());
                    userObj.BrokerPicName = dr["BrokerPicName"].ToString();
                    userObj.IsNewReleaseFlag = dr["IsNewReleaseFlag"].ToString();
                    userObj.LoginCompanyId = dr["LoginCompanyId"].ToString();
                    userObj.BrokerDivisionCode = Convert.ToString(dr["BrokerDivisionCode"].ToString());
                    userObj.DeliveryCharge = Convert.ToString(dr["DeliveryCharge"].ToString());
                    userObj.MinimumDeliveryChargeAmount = dr["DeliveryCharge"].ToString();
                    userObj.IsCreatePost = Convert.ToBoolean(dr["IsCreatePost"]);
                }

                log.Debug("Debug logging: GetUserProfile");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }
        public UserDetailsDto GetUserProfile(LogindBindingModel model, string apiConnectionAuthString)
        {
            UserDetailsDto userObj = null;

            try
            {
                userObj = new UserDetailsDto();
                //_con = new SqlConnection(apiConnectionAuthString);
                dbConnectionObj = new DBConnection(apiConnectionAuthString);
                com = new SqlCommand(ApiConstant.GetLoginUserProfile, dbConnectionObj.GetApiConnection());
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@UserName", model.UserName);
                com.Parameters.AddWithValue("@UserPassword", model.Password);
                da = new SqlDataAdapter(com);
                dt = new DataTable();
                da.Fill(dt);

                foreach (DataRow dr in dt.Rows)
                {
                    userObj.UserName = Convert.ToString(dr["UserLoginID"]);
                    userObj.UserFullName = Convert.ToString(dr["Name"]);
                    userObj.Email = Convert.ToString(dr["UserLoginID"]);
                    userObj.PhoneNumber = Convert.ToString(dr["ContactNo"]);
                    userObj.Domain = Convert.ToString(dr["Domain"]);
                    userObj.Role = Convert.ToString(dr["UserRoleName"]);
                    userObj.RoleId = Convert.ToString(dr["UserRole"]);
                    userObj.UserRoleName = Convert.ToString(dr["UserRoleName"]);
                    userObj.CompanyId = Convert.ToString(dr["CompanyId"]);
                    userObj.CompanyName = Convert.ToString(dr["CompanyName"]);
                    userObj.UserType = Convert.ToString(dr["UserTypeCode"]);
                    userObj.BrokerPicName = Convert.ToString(dr["BrokerPicName"]);
                    userObj.IsNewReleaseFlag = Convert.ToString(dr["IsNewReleaseFlag"]);
                    userObj.LoginCompanyId = Convert.ToString(dr["CompanyId"]);
                    userObj.BrokerDivisionCode = Convert.ToString(dr["BrokerDivisionCode"]);
                    userObj.DeliveryCharge = Convert.ToString(dr["DeliveryCharge"]);
                    userObj.MinimumDeliveryChargeAmount = Convert.ToString(dr["MinimumDeliveryChargeAmount"]);
                    userObj.IsCreatePost = true;
                    userObj.Address = Convert.ToString(dr["Address"]);
                    userObj.City = Convert.ToString(dr["City"]);
                    userObj.State = Convert.ToString(dr["State"]);
                    userObj.Zip = Convert.ToString(dr["Zip"]);
                    userObj.CountryId = Convert.ToInt32(dr["CountryId"]);
                    userObj.CountryName = Convert.ToString(dr["CountryName"]);

                    //userObj.UserLoginID = dr["UserLoginID"].ToString();
                    //userObj.UserId = Convert.ToInt32(dr["UserId"]);
                    ////userObj.UserFullName = dr["UserFullName"].ToString();
                    //userObj.CompanyId = Convert.ToInt32(dr["CompanyId"].ToString());
                    //userObj.Name = dr["Name"].ToString();
                    //userObj.Address = dr["Address"].ToString();
                    //userObj.Email = dr["Email"].ToString();
                    //userObj.ContactNo = dr["ContactNo"].ToString();
                    //userObj.City = dr["City"].ToString();
                    //userObj.State = Convert.ToString(dr["State"].ToString());
                    //userObj.Zip = dr["Zip"].ToString();
                    //userObj.UserLoginID = dr["UserLoginID"].ToString();
                    ////userObj.UserPassword = dr["UserPassword"].ToString();
                    //userObj.UserRole = Convert.ToInt32(dr["UserRole"].ToString());
                    //userObj.IsActive = Convert.ToBoolean(dr["IsActive"].ToString());
                    //userObj.DefaultLanguage = dr["DefaultLanguage"].ToString();
                    ////userObj.IMEI = dr["IMEI"].ToString();
                    //userObj.CreatedOn = Convert.ToDateTime(dr["CreatedOn"].ToString());
                    //userObj.DateAuthentication = dr["DateAuthentication"] == DBNull.Value ? DateTime.MinValue : Convert.ToDateTime(dr["DateAuthentication"]);
                }

                log.Debug("Debug logging: GetUserProfile");

                return userObj;
            }
            catch (Exception ex)
            {
                log.Error("Error: " + ex);
            }
            finally
            {
                com = null;
                da = null;
                dt = null;
            }

            return userObj;
        }

        #endregion        


    }
}
