﻿using Microsoft.EntityFrameworkCore;
using Sales.EfCore.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Sales.Infra.Interfaces;

namespace Sales.Repositories.Implementation
{
    public class BaseRepository<T> : IRepository<T> where T : class
    {
        protected readonly DBContext _context;
        //protected readonly TestGoyaDB2Context _db2context;
        private readonly DbSet<T> _dbSet;
        protected BaseRepository(DBContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public void AddAsync(T entity)
        {
            _context.Set<T>().AddAsync(entity);
        }

        public void AddRange(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        public int Commit()
        {
            int result = 0;
            try
            {
                result = this._context.SaveChanges();
            }
            catch (Exception ex)
            {
            }
            return result;
        }
        public async Task<IEnumerable<T>> FindAllAsync(Expression<Func<T, bool>> predicate)
        {
            var findAll = await _context.Set<T>().Where(predicate).ToListAsync();
            return findAll.AsEnumerable();
        }
        public async Task<List<T>> GetAllAsync()
        {
            var getAll = await _context.Set<T>().ToListAsync();

            return getAll;
        }
        public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {
            IQueryable<T> set = _context.Set<T>().Where(predicate);

            return await includes.Aggregate(set, (current, include) => current.Include(include)).ToListAsync();

            // return (await includes.Aggregate(set, (current, include) => current.Include(include)).ToListAsync() ?? default(IEnumerable<T>));
        }
        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includes)
        {

            IQueryable<T> set = _context.Set<T>().Where(predicate);

            return (await includes.Aggregate(set, (current, include) => current.Include(include)).FirstOrDefaultAsync() ?? default(T));
        }
        public async Task<T> GetAsync(int id)
        {
            T o = await _context.Set<T>().FindAsync(id);
            if (o == null)
            {
                throw new Exception("Record Not Found", new KeyNotFoundException("Record Not Found"));
            }
            return o;
        }

        public void Remove(T entity)
        {
            _context.Entry(entity).State = EntityState.Deleted;
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }
        public void UpdateAsync(T entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
        public bool Exists(Func<T, bool> conditions)
        {
            return _dbSet.Any(conditions);
        }


        /// <summary>
        /// Uses raw SQL queries to fetch the specified <typeparamref name="TEntity" /> data.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        /// <param name="sql">The raw SQL.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns>An <see cref="IQueryable{T}" /> that contains elements that satisfy the condition specified by raw SQL.</returns>
        public IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class => _context.Set<TEntity>().FromSqlRaw(sql, parameters);


        /// <summary>
        /// Saves all changes made in this context to the database.
        /// </summary>        
        /// <returns>The number of state entries written to the database.</returns>
        public int SaveChanges(bool ensureAutoHistory = false)
        {
            return _context.SaveChanges();
        }
    }
}
