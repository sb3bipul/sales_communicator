﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Sales.DTO.Models;
using Sales.DTO.Models.RequestModel;
using Sales.EfCore.DataContext;
using Sales.EfCore.Entity;
using Sales.Infra.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using Microsoft.AspNetCore.Http;
using System.Web;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.FileProviders;
using Sales.Repositories.Helper;
using System.Data;
using System.Reflection;

namespace Sales.Repositories.Implementation
{
    public class MyPostRepository : BaseRepository<tblm_PostType>, IMyPost
    {
        IConfiguration configuration;
        public MyPostRepository(DBContext context, IConfiguration configuration) : base(context)
        {
            context.Database.SetCommandTimeout(1);
            this.configuration = configuration;
        }

        /// <summary>
        /// Get post type data
        /// </summary>
        /// <returns></returns>
        public async Task<List<PostTypeDto>> GetPostTypeList()
        {
            List<PostTypeDto> posttype = new List<PostTypeDto>();
            try
            {
                DBContext _context = new DBContext();
                posttype = await
                _context.Set<PostTypeDto>().FromSqlRaw("SProc_Sales_GetPostType").ToListAsync();
            }
            catch (Exception ex)
            {
                posttype = null;
            }
            return posttype;
        }

        /// <summary>
        /// Get Privacy list
        /// </summary>
        /// <returns></returns>
        public async Task<List<PrivacyTypeDto>> GetPrivacyTypeList()
        {
            List<PrivacyTypeDto> privacytype = new List<PrivacyTypeDto>();
            try
            {
                DBContext _context = new DBContext();
                privacytype = await
                _context.Set<PrivacyTypeDto>().FromSqlRaw("SProc_Sales_GetPrivacyType").ToListAsync();
            }
            catch (Exception ex)
            {
                privacytype = null;
            }
            return privacytype;
        }

        public async Task<List<UserPostDto>> InsertUserPostData(UserPostModel model)
        {
            List<UserPostDto> SubmitList = new List<UserPostDto>();
            List<AddNotification> addNotification = new List<AddNotification>();
            try
            {
                SqlParameter CreateBy = new SqlParameter("@CREATEDBY", model.CREATEDBY);
                SqlParameter PrivacyType = new SqlParameter("@PRIVACYTYPEID", model.PRIVACYTYPEID);
                SqlParameter PostType = new SqlParameter("@POSTTYPEID", model.POSTTYPEID);
                SqlParameter Comments = new SqlParameter("@COMMENTS", model.COMMENTS);
                SqlParameter FromDate = new SqlParameter("@FROMDATE", model.FROMDATE);
                SqlParameter ToDate = new SqlParameter("@TODATE", model.TODATE);
                SqlParameter Header = new SqlParameter("@Header", model.Header);
                SqlParameter CompanyId = new SqlParameter("@CompanyId", string.IsNullOrEmpty(model.CompanyId) ? DBNull.Value : (object)model.CompanyId);
                SqlParameter DivisionId = new SqlParameter("@DivisionId", string.IsNullOrEmpty(model.DivisionId) ? DBNull.Value : (object)model.DivisionId);
                SqlParameter GroupId = new SqlParameter("@GroupId", string.IsNullOrEmpty(model.GroupId) ? DBNull.Value : (object)model.GroupId);
                SqlParameter Zone = new SqlParameter("@Zone", string.IsNullOrEmpty(model.Zone) ? DBNull.Value : (object)model.Zone);
                DBContext _context = new DBContext();
                SubmitList = await
                _context.Set<UserPostDto>().FromSqlRaw("SProc_Sales_InsertUserPostData  @CREATEDBY,@PRIVACYTYPEID,@POSTTYPEID,@COMMENTS,@FROMDATE,@TODATE,@Header,@CompanyId,@DivisionId,@GroupId",
                CreateBy, PrivacyType, PostType, Comments, FromDate, ToDate, Header, CompanyId, DivisionId, GroupId).ToListAsync();
                if (SubmitList.Count > 0)
                {
                    List<NotificID> userList = new List<NotificID>();
                    var _postNo = SubmitList[0].POSTNO;
                    SqlParameter postNo = new SqlParameter("@postNo", _postNo);
                    //SqlParameter tagUser = new SqlParameter("@tagUser", "");
                    userList = await _context.Set<NotificID>().FromSqlRaw("SProc_Sales_InsertNotification @postNo,@CREATEDBY,@Header,@CompanyId,@DivisionId,@GroupId,@Zone", postNo, CreateBy, Header, CompanyId, DivisionId, GroupId, Zone).ToListAsync();

                }
            }
            catch (Exception ex)
            {
                throw (ex);
                SubmitList = null;
            }
            return SubmitList;
        }


        public async Task<List<UserPostDto>> InsertUserPostMediaData(UserPostMediaModel model)
        {
            List<UserPostDto> SubmitList = new List<UserPostDto>();
            try
            {
                string StoreImgPath = string.Empty;
                var saveImageName = "";
                var savePdfName = "";
                var ExcelName = "";
                var savePPTName = "";
                System.Drawing.Image img;
                //String StoreImgPath = ("~/ComplaintImage");
                //String StoreImgPath = 
                StoreImgPath = configuration["ImageSavePath:physicalPath"];
                string SavePath = configuration["ImageSavePath:Path"];


                string imageSavePath = string.Empty;
                var base64Image = model.MediaBase64;
                var imageName = model.MEDIAFILENAME;

                if (!String.IsNullOrEmpty(base64Image))
                {
                    if (model.IsMobile)
                    {
                        saveImageName = imageName;
                    }
                    else
                    {
                        saveImageName = model.POSTNO + model.ct + Path.GetExtension(imageName);
                    }
                    imageSavePath = StoreImgPath + "\\" + saveImageName;
                    if (!String.IsNullOrEmpty(base64Image))
                    {
                        try
                        {
                            img = Base64ToImage(Convert.ToString(base64Image));
                            img.Save(imageSavePath);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                }

                ///--- for pdf
                if (model.PdfBase64 != null)
                {
                    string PdfSavePath = string.Empty;

                    var base64Pdf = model.PdfBase64;
                    var PdfName = model.pdfName;
                    if (!String.IsNullOrEmpty(base64Pdf))
                    {
                        savePdfName = model.POSTNO + Path.GetExtension(PdfName);
                        PdfSavePath = StoreImgPath + "\\" + savePdfName;
                        if (!String.IsNullOrEmpty(base64Pdf))
                        {
                            byte[] PDFDecoded = Convert.FromBase64String(base64Pdf);
                            FileStream obj = File.Create(PdfSavePath);
                            obj.Write(PDFDecoded, 0, PDFDecoded.Length);
                            obj.Flush();
                            obj.Close();
                        }
                    }
                }

                ///--- for excel
                if (model.excelFileData != null)
                {
                    if (!String.IsNullOrEmpty(model.excelFileData))
                    {
                        ExcelName = model.POSTNO + Path.GetExtension(model.excelFileName);
                        string FileSaveWithPath = StoreImgPath + "\\" + ExcelName;//WebConfigurationManager.AppSettings["UploadFileData"] + customerId + "@" + fileName;
                        string base64BinaryStr = model.excelFileData;
                        if (!String.IsNullOrEmpty(model.excelFileData))
                        {
                            byte[] bytes = Convert.FromBase64String(base64BinaryStr);
                            System.IO.FileStream stream = new FileStream(FileSaveWithPath, FileMode.Append);
                            System.IO.BinaryWriter writer = new BinaryWriter(stream);
                            writer.Write(bytes, 0, bytes.Length);
                            writer.Close();
                        }
                    }
                }

                if (model.pptFileData != null)
                {
                    string PptSavePath = string.Empty;

                    var base64ppt = model.pptFileData;
                    var PptName = model.pptFileName;
                    if (!String.IsNullOrEmpty(base64ppt))
                    {
                        savePPTName = model.POSTNO + Path.GetExtension(PptName);
                        PptSavePath = StoreImgPath + "\\" + savePPTName;
                        if (!String.IsNullOrEmpty(base64ppt))
                        {
                            byte[] PPTDecoded = Convert.FromBase64String(base64ppt);
                            FileStream obj = File.Create(PptSavePath);
                            obj.Write(PPTDecoded, 0, PPTDecoded.Length);
                            obj.Flush();
                            obj.Close();
                        }
                    }
                }

                SqlParameter PostNo = new SqlParameter("@POSTNO", model.POSTNO);
                SqlParameter MediaName = new SqlParameter("@MEDIAFILENAME", saveImageName);
                SqlParameter excel = new SqlParameter("@ExcelFileName", ExcelName);
                SqlParameter ppt = new SqlParameter("@PPTName", savePPTName);
                SqlParameter pdf = new SqlParameter("@PDFName", savePdfName);
                SqlParameter FilePath = new SqlParameter("@FILEURL", SavePath);
                DBContext _context = new DBContext();
                SubmitList = await
                _context.Set<UserPostDto>().FromSqlRaw("SProc_Sales_InsertUserPostMedia  @POSTNO,@MEDIAFILENAME,@ExcelFileName,@PPTName,@PDFName,@FILEURL", PostNo, MediaName, excel, ppt, pdf, FilePath).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
                SubmitList = null;
            }
            return SubmitList;
        }

        public System.Drawing.Image Base64ToImage(string base64String)
        {
            // Convert Base64 String to byte[]
            byte[] imageBytes = Convert.FromBase64String(base64String);
            MemoryStream ms = new MemoryStream(imageBytes, 0,
              imageBytes.Length);

            // Convert byte[] to Image
            ms.Write(imageBytes, 0, imageBytes.Length);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
            return image;
        }


        public Image LoadImage(string base64String)
        {
            try
            {
                //data:image/gif;base64,
                //this image is a single pixel (black)
                byte[] bytes = Convert.FromBase64String(base64String);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                return image;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        //api/GetUserPoseMediadata
        public async Task<List<PostDataMediaDto>> GetUserPoseMediadata(PostMediaModel model)
        {
            List<PostDataDto> postdata = new List<PostDataDto>();
            List<PostDataMediaDto> listpostdatamedia = new List<PostDataMediaDto>();
            List<PostMediaDto> lstpm = new List<PostMediaDto>();
            try
            {
                SqlParameter CreatedBy = new SqlParameter("@CREATEDBY", string.IsNullOrEmpty(model.CREATEDBY) ? DBNull.Value : (object)model.CREATEDBY);
                SqlParameter FromDate = new SqlParameter("@FROMDATE", model.FROMDATE);
                SqlParameter ToDate = new SqlParameter("@TODATE", model.TODATE);
                SqlParameter PrivacyType = new SqlParameter("@PRIVACYTYPE", string.IsNullOrEmpty(model.PRIVACYTYPEID) ? DBNull.Value : (object)model.PRIVACYTYPEID);
                SqlParameter PostType = new SqlParameter("@POSTTYPE", string.IsNullOrEmpty(model.POSTTYPEID) ? DBNull.Value : (object)model.POSTTYPEID);
                SqlParameter TagText = new SqlParameter("@TagText", string.IsNullOrEmpty(model.TagText) ? DBNull.Value : (object)model.TagText);
                SqlParameter UserName = new SqlParameter("@UserName", string.IsNullOrEmpty(model.UserName) ? DBNull.Value : (object)model.UserName);
                SqlParameter UserRole = new SqlParameter("@UserRole", string.IsNullOrEmpty(model.UserRole) ? DBNull.Value : (object)model.UserRole);
                //SqlParameter PageNumber = new SqlParameter("@PageNumber", model.PageNumber <= 0 ? 1 : (object)model.PageNumber);
                //SqlParameter PageSize = new SqlParameter("@PageSize", model.PageSize <= 0 ? 100 : (object)model.PageSize);

                DBContext _context = new DBContext();
                postdata = await
                _context.Set<PostDataDto>().FromSqlRaw("SProc_Sales_GetUserMediaData  @CREATEDBY,@FROMDATE,@TODATE,@PRIVACYTYPE,@POSTTYPE,@TagText,@UserName,@UserRole", CreatedBy, FromDate, ToDate, PrivacyType, PostType, TagText, UserName, UserRole).ToListAsync();
                #region manipulate postdat for response PostDataMediaDto
                string preostNo = "";
                string excelfile = "";
                string PdfFile = "";
                string pptFile = "";
                PostDataMediaDto pdmdto = null;
                PostMediaDto pmd = null;
                for (int i = 0; i < postdata.Count; i++)
                {
                    if (postdata[i].POSTNO != preostNo)
                    {
                        if (preostNo != "")
                        {
                            pdmdto.POSTMEDIAS = lstpm;
                            listpostdatamedia.Add(pdmdto);
                            lstpm = new List<PostMediaDto>();
                        }
                        //pmd = new PostMediaDto();
                        //pmd.POSTNO = postdata[i].POSTNO;
                        //pmd.POSTMEDIA = postdata[i].POSTMEDIA;
                        //lstpm.Add(pmd);
                    }
                    pdmdto = new PostDataMediaDto();
                    pdmdto.POSTNO = postdata[i].POSTNO;
                    pdmdto.CREATEDBY = postdata[i].CREATEDBY;
                    pdmdto.CREATEDON = Convert.ToString(postdata[i].CREATEDON);
                    pdmdto.PRIVACYTYPE = postdata[i].PRIVACYTYPE;
                    pdmdto.POSTTYPE = postdata[i].POSTTYPE;
                    pdmdto.COMMENTS = postdata[i].COMMENTS;
                    pdmdto.FROMDATE = postdata[i].FROMDATE;
                    pdmdto.TODATE = postdata[i].TODATE;
                    pdmdto.Header = postdata[i].Header;
                    pdmdto.CommentCount = postdata[i].CommentCount;
                    pmd = new PostMediaDto();
                    //pmd.POSTNO = postdata[i].POSTNO;
                    //pmd.POSTMEDIA = postdata[i].POSTMEDIA;
                    pmd.image = postdata[i].POSTMEDIA;
                    pmd.thumbImage = postdata[i].POSTMEDIA;
                    pmd.title = "";
                    pmd.alt = "";
                    lstpm.Add(pmd);

                    if (i == postdata.Count - 1)
                    {
                        pdmdto.POSTMEDIAS = lstpm;
                        listpostdatamedia.Add(pdmdto);
                    }

                    //if (postdata.Count == 1)
                    //{
                    //    pdmdto.POSTMEDIAS = lstpm;
                    //    listpostdatamedia.Add(pdmdto);
                    //}

                    if (postdata[i].Excelmedia != excelfile || postdata[i].Excelmedia != null)
                    {
                        pdmdto.Excelmedia = Convert.ToString(postdata[i].Excelmedia);
                    }

                    if (postdata[i].Pdfmedia != PdfFile || postdata[i].Pdfmedia != null)
                    {
                        pdmdto.Pdfmedia = Convert.ToString(postdata[i].Pdfmedia);
                    }

                    if (postdata[i].PPTmedia != pptFile || postdata[i].PPTmedia != null)
                    {
                        pdmdto.PPTmedia = Convert.ToString(postdata[i].PPTmedia);
                    }


                    preostNo = postdata[i].POSTNO;
                    excelfile = Convert.ToString(postdata[i].Excelmedia);
                    PdfFile = Convert.ToString(postdata[i].Pdfmedia);
                    pptFile = Convert.ToString(postdata[i].PPTmedia);
                }


                #endregion
            }
            catch (Exception ex)
            {
                throw (ex);
                postdata = null;
            }
            return listpostdatamedia;
        }

        public async Task<List<CompanyDto>> GetCompanyList(string CompanyCode)
        {
            List<CompanyDto> company = new List<CompanyDto>();
            SqlParameter CompanyId = new SqlParameter("@CompanyId", string.IsNullOrEmpty(CompanyCode) ? DBNull.Value : (object)CompanyCode);
            try
            {
                DBContext _context = new DBContext();
                company = await
                _context.Set<CompanyDto>().FromSqlRaw("SP_GetAllCompany @CompanyId", CompanyId).ToListAsync();
            }
            catch (Exception ex)
            {
                company = null;
            }
            return company;
        }

        public async Task<List<BrokerGroupDto>> GetGroupByCompany(GroupModel Model)
        {
            List<BrokerGroupDto> Divcompany = new List<BrokerGroupDto>();
            SqlParameter CompanyId = new SqlParameter("@CompanyId", string.IsNullOrEmpty(Model.CompanyId) ? DBNull.Value : (object)Model.CompanyId);
            SqlParameter Division = new SqlParameter("@DivisionId", string.IsNullOrEmpty(Model.DivisionId) ? DBNull.Value : (object)Model.DivisionId);
            SqlParameter Group = new SqlParameter("@GroupId", string.IsNullOrEmpty(Model.GroupId) ? DBNull.Value : (object)Model.GroupId);
            try
            {
                DBContext _context = new DBContext();
                Divcompany = await
                _context.Set<BrokerGroupDto>().FromSqlRaw("SP_GetGroupByDivisionCompany @CompanyId,@DivisionId,@GroupId", CompanyId, Division, Group).ToListAsync();
            }
            catch (Exception ex)
            {
                Divcompany = null;
            }
            return Divcompany;
        }

        public async Task<List<DivisionCompanyDto>> GetDivisionByCompany(DivisionModel Model)
        {
            List<DivisionCompanyDto> Divcompany = new List<DivisionCompanyDto>();
            SqlParameter CompanyId = new SqlParameter("@CompanyId", string.IsNullOrEmpty(Model.CompanyId) ? DBNull.Value : (object)Model.CompanyId);
            SqlParameter Division = new SqlParameter("@DivisionId", string.IsNullOrEmpty(Model.DivisionId) ? DBNull.Value : (object)Model.DivisionId);

            try
            {
                DBContext _context = new DBContext();
                Divcompany = await
                _context.Set<DivisionCompanyDto>().FromSqlRaw("SP_GetDivisionsByCompanyId @CompanyId,@DivisionId", CompanyId, Division).ToListAsync();
            }
            catch (Exception ex)
            {
                Divcompany = null;
            }
            return Divcompany;
        }

        public async Task<List<ZoneDto>> GetAllZone(ZoneModel model)
        {
            List<ZoneDto> zonelist = new List<ZoneDto>();
            SqlParameter CompanyId = new SqlParameter("@CompanyID", string.IsNullOrEmpty(model.CompanyId) ? DBNull.Value : (object)model.CompanyId);
            try
            {
                DBContext _context = new DBContext();
                zonelist = await
                _context.Set<ZoneDto>().FromSqlRaw("SP_GetZoneList @CompanyID", CompanyId).ToListAsync();
            }
            catch (Exception ex)
            {
                zonelist = null;
            }
            return zonelist;
        }

        public async Task<List<PostLocationDto>> InsertUserPostLocation(UserPostLocation model)
        {
            List<PostLocationDto> location = new List<PostLocationDto>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@POSTNO", string.IsNullOrEmpty(model.POSTNO) ? DBNull.Value : (object)model.POSTNO);
                SqlParameter LocationId = new SqlParameter("@LOCATIONID", string.IsNullOrEmpty(model.POSTLOCATION) ? DBNull.Value : (object)model.POSTLOCATION);

                DBContext _context = new DBContext();
                location = await
                _context.Set<PostLocationDto>().FromSqlRaw("SProc_Insert_UserPostLocation  @POSTNO,@LOCATIONID", PostNo, LocationId).ToListAsync();
            }
            catch (Exception ex)
            {
                //throw (ex);
                location = null;
            }
            return location;
        }

        public async Task<List<PostTagDto>> InsertUserPostTag(UserPostTag model)
        {
            List<PostTagDto> postTag = new List<PostTagDto>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@POSTNO", string.IsNullOrEmpty(model.POstNo) ? DBNull.Value : (object)model.POstNo);
                SqlParameter UserName = new SqlParameter("@USERNAME", string.IsNullOrEmpty(model.UserName) ? DBNull.Value : (object)model.UserName);
                SqlParameter Location = new SqlParameter("@TAGLOCATIONID", string.IsNullOrEmpty(model.TagLocation) ? DBNull.Value : (object)model.TagLocation);

                DBContext _context = new DBContext();
                postTag = await
                _context.Set<PostTagDto>().FromSqlRaw("SProc_Insert_UserPostTagDetails  @POSTNO,@USERNAME,@TAGLOCATIONID", PostNo, UserName, Location).ToListAsync();
            }
            catch (Exception ex)
            {
                //throw (ex);
                postTag = null;
            }
            return postTag;
        }


        public async Task<List<PostTagDto>> MakePostFavourite(UserPostTag model)
        {
            List<PostTagDto> postTag = new List<PostTagDto>();
            try
            {
                SqlParameter PostNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(model.POstNo) ? DBNull.Value : (object)model.POstNo);
                SqlParameter UserName = new SqlParameter("@USERNAME", string.IsNullOrEmpty(model.UserName) ? DBNull.Value : (object)model.UserName);
                DBContext _context = new DBContext();
                postTag = await
                _context.Set<PostTagDto>().FromSqlRaw("SProc_Sales_MakePostFavourite @PostNo,@USERNAME", PostNo, UserName).ToListAsync();
            }
            catch (Exception ex)
            {
                //throw (ex);
                postTag = null;
            }
            return postTag;
        }

        public async Task<List<PostDataMediaDto>> GetUserFavoritePoseMediadata(PostMediaModel model)
        {
            List<PostDataDto> postdata = new List<PostDataDto>();
            List<PostDataMediaDto> listpostdatamedia = new List<PostDataMediaDto>();
            List<PostMediaDto> lstpm = new List<PostMediaDto>();
            try
            {
                SqlParameter CreatedBy = new SqlParameter("@CREATEDBY", string.IsNullOrEmpty(model.CREATEDBY) ? DBNull.Value : (object)model.CREATEDBY);
                SqlParameter FromDate = new SqlParameter("@FROMDATE", model.FROMDATE);
                SqlParameter ToDate = new SqlParameter("@TODATE", model.TODATE);
                SqlParameter PrivacyType = new SqlParameter("@PRIVACYTYPE", string.IsNullOrEmpty(model.PRIVACYTYPEID) ? DBNull.Value : (object)model.PRIVACYTYPEID);
                SqlParameter PostType = new SqlParameter("@POSTTYPE", string.IsNullOrEmpty(model.POSTTYPEID) ? DBNull.Value : (object)model.POSTTYPEID);
                SqlParameter TagText = new SqlParameter("@TagText", string.IsNullOrEmpty(model.TagText) ? DBNull.Value : (object)model.TagText);
                SqlParameter UserName = new SqlParameter("@UserName", string.IsNullOrEmpty(model.UserName) ? DBNull.Value : (object)model.UserName);
                SqlParameter UserRole = new SqlParameter("@UserRole", string.IsNullOrEmpty(model.UserRole) ? DBNull.Value : (object)model.UserRole);
                SqlParameter Favorite = new SqlParameter("@IsFavorite", model.IsFavorite);


                DBContext _context = new DBContext();
                postdata = await
                _context.Set<PostDataDto>().FromSqlRaw("SProc_Sales_GetUserFavoriteMediaData  @CREATEDBY,@FROMDATE,@TODATE,@PRIVACYTYPE,@POSTTYPE,@TagText,@UserName,@UserRole,@IsFavorite", CreatedBy, FromDate, ToDate, PrivacyType, PostType, TagText, UserName, UserRole, Favorite).ToListAsync();
                #region manipulate postdat for response PostDataMediaDto
                string preostNo = "";
                string excelfile = "";
                string PdfFile = "";
                string pptFile = "";
                PostDataMediaDto pdmdto = null;
                PostMediaDto pmd = null;
                for (int i = 0; i < postdata.Count; i++)
                {
                    if (postdata[i].POSTNO != preostNo)
                    {
                        if (preostNo != "")
                        {
                            pdmdto.POSTMEDIAS = lstpm;
                            listpostdatamedia.Add(pdmdto);
                            lstpm = new List<PostMediaDto>();
                        }
                        //pmd = new PostMediaDto();
                        //pmd.POSTNO = postdata[i].POSTNO;
                        //pmd.POSTMEDIA = postdata[i].POSTMEDIA;
                        //lstpm.Add(pmd);
                    }
                    pdmdto = new PostDataMediaDto();
                    pdmdto.POSTNO = postdata[i].POSTNO;
                    pdmdto.CREATEDBY = postdata[i].CREATEDBY;
                    pdmdto.CREATEDON = Convert.ToString(postdata[i].CREATEDON);
                    pdmdto.PRIVACYTYPE = postdata[i].PRIVACYTYPE;
                    pdmdto.POSTTYPE = postdata[i].POSTTYPE;
                    pdmdto.COMMENTS = postdata[i].COMMENTS;
                    pdmdto.FROMDATE = postdata[i].FROMDATE;
                    pdmdto.TODATE = postdata[i].TODATE;
                    pdmdto.Header = postdata[i].Header;
                    pmd = new PostMediaDto();
                    //pmd.POSTNO = postdata[i].POSTNO;
                    //pmd.POSTMEDIA = postdata[i].POSTMEDIA;
                    pmd.image = postdata[i].POSTMEDIA;
                    pmd.thumbImage = postdata[i].POSTMEDIA;
                    pmd.title = "";
                    pmd.alt = "";
                    lstpm.Add(pmd);

                    if (i == postdata.Count - 1)
                    {
                        pdmdto.POSTMEDIAS = lstpm;
                        listpostdatamedia.Add(pdmdto);
                    }

                    //if (postdata.Count == 1)
                    //{
                    //    pdmdto.POSTMEDIAS = lstpm;
                    //    listpostdatamedia.Add(pdmdto);
                    //}

                    if (postdata[i].Excelmedia != excelfile || postdata[i].Excelmedia != null)
                    {
                        pdmdto.Excelmedia = Convert.ToString(postdata[i].Excelmedia);
                    }

                    if (postdata[i].Pdfmedia != PdfFile || postdata[i].Pdfmedia != null)
                    {
                        pdmdto.Pdfmedia = Convert.ToString(postdata[i].Pdfmedia);
                    }

                    if (postdata[i].PPTmedia != pptFile || postdata[i].PPTmedia != null)
                    {
                        pdmdto.PPTmedia = Convert.ToString(postdata[i].PPTmedia);
                    }


                    preostNo = postdata[i].POSTNO;
                    excelfile = Convert.ToString(postdata[i].Excelmedia);
                    PdfFile = Convert.ToString(postdata[i].Pdfmedia);
                    pptFile = Convert.ToString(postdata[i].PPTmedia);
                }


                #endregion
            }
            catch (Exception ex)
            {
                throw (ex);
                postdata = null;
            }
            return listpostdatamedia;
        }

        public async Task<List<NotificID>> GetUserListByZoneGroupDiv(GetUserListByZoneGroupDiv model)
        {
            List<NotificID> userList = new List<NotificID>();
            try
            {
                SqlParameter zone = new SqlParameter("@Zone", string.IsNullOrEmpty(model.Zone) ? DBNull.Value : (object)model.Zone);
                SqlParameter groupId = new SqlParameter("@GroupId", string.IsNullOrEmpty(model.GroupId) ? DBNull.Value : (object)model.GroupId);
                SqlParameter divisionId = new SqlParameter("@DivisionId", string.IsNullOrEmpty(model.DivisionId) ? DBNull.Value : (object)model.DivisionId);
                SqlParameter postNo = new SqlParameter("@PostNo", string.IsNullOrEmpty(model.PostNo) ? DBNull.Value : (object)model.PostNo);
                //GetPostDetails(model.PostNo);
                DBContext _context = new DBContext();
                userList = await _context.Set<NotificID>().FromSqlRaw("SProc_Sales_GetUserListByZoneGroupDiv @Zone,@GroupId,@DivisionId", zone, groupId, divisionId).ToListAsync();
                //if (userList.Count > 0)
                //{
                //    foreach (var k in userList)
                //    {
                //        var post = new List<AddNotification>();
                //        var _user = k.UserName;
                //        List<UserPostModel> post = new List<UserPostModel>();
                //        SqlParameter _postNo = new SqlParameter("@PostNo", postNo);
                //        post = await _context.Set<AddNotification>().FromSqlRaw("SProc_Sales_InsertNotification @Zone,@GroupId,@DivisionId", zone, groupId, divisionId).ToListAsync();


                //    }
                //}
            }
            catch (Exception ex)
            {
                userList = null;
            }
            return userList;

        }
        //public async Task<List<UserPostModel>> GetPostDetails(string PostNo)
        //{
        //    List<UserPostModel> post = new List<UserPostModel>();
        //    SqlParameter postNo = new SqlParameter("@PostNo", PostNo);
        //    DBContext _context = new DBContext();
        //    post = await _context.Set<NotificID>().FromSqlRaw("SProc_Sales_GetUserListByZoneGroupDiv @Zone,@GroupId,@DivisionId", zone, groupId, divisionId).ToListAsync();
        //    return post;
        //}
        public async Task<List<PostDataMediaDto>> GetUserPoseMediadataNotification(NotificID model)
        {
            List<PostDataDto> postdata = new List<PostDataDto>();
            List<PostDataMediaDto> listpostdatamedia = new List<PostDataMediaDto>();
            List<PostMediaDto> lstpm = new List<PostMediaDto>();
            try
            {
                SqlParameter _UserName = new SqlParameter("@UserName", model.UserName);

                DBContext _context = new DBContext();
                postdata = await
                _context.Set<PostDataDto>().FromSqlRaw("SProc_GetNotificationList @UserName", _UserName).ToListAsync();

                #region manipulate postdat for response PostDataMediaDto
                string preostNo = "";
                string excelfile = "";
                string PdfFile = "";
                string pptFile = "";
                PostDataMediaDto pdmdto = null;
                PostMediaDto pmd = null;
                for (int i = 0; i < postdata.Count; i++)
                {
                    if (postdata[i].POSTNO != preostNo)
                    {
                        if (preostNo != "")
                        {
                            pdmdto.POSTMEDIAS = lstpm;
                            listpostdatamedia.Add(pdmdto);
                            lstpm = new List<PostMediaDto>();
                        }
                    }
                    pdmdto = new PostDataMediaDto();
                    pdmdto.POSTNO = postdata[i].POSTNO;
                    pdmdto.CREATEDBY = postdata[i].CREATEDBY;
                    pdmdto.CREATEDON = Convert.ToString(postdata[i].CREATEDON);
                    pdmdto.PRIVACYTYPE = postdata[i].PRIVACYTYPE;
                    pdmdto.POSTTYPE = postdata[i].POSTTYPE;
                    pdmdto.COMMENTS = postdata[i].COMMENTS;
                    pdmdto.FROMDATE = postdata[i].FROMDATE;
                    pdmdto.TODATE = postdata[i].TODATE;
                    pdmdto.Header = postdata[i].Header;
                    pmd = new PostMediaDto();
                    pmd.image = postdata[i].POSTMEDIA;
                    pmd.thumbImage = postdata[i].POSTMEDIA;
                    pmd.title = "";
                    pmd.alt = "";
                    lstpm.Add(pmd);

                    if (i == postdata.Count - 1)
                    {
                        pdmdto.POSTMEDIAS = lstpm;
                        listpostdatamedia.Add(pdmdto);
                    }

                    //if (postdata.Count == 1)
                    //{
                    //    pdmdto.POSTMEDIAS = lstpm;
                    //    listpostdatamedia.Add(pdmdto);
                    //}

                    if (postdata[i].Excelmedia != excelfile || postdata[i].Excelmedia != null)
                    {
                        pdmdto.Excelmedia = Convert.ToString(postdata[i].Excelmedia);
                    }

                    if (postdata[i].Pdfmedia != PdfFile || postdata[i].Pdfmedia != null)
                    {
                        pdmdto.Pdfmedia = Convert.ToString(postdata[i].Pdfmedia);
                    }

                    if (postdata[i].PPTmedia != pptFile || postdata[i].PPTmedia != null)
                    {
                        pdmdto.PPTmedia = Convert.ToString(postdata[i].PPTmedia);
                    }


                    preostNo = postdata[i].POSTNO;
                    excelfile = Convert.ToString(postdata[i].Excelmedia);
                    PdfFile = Convert.ToString(postdata[i].Pdfmedia);
                    pptFile = Convert.ToString(postdata[i].PPTmedia);
                }


                #endregion
            }
            catch (Exception ex)
            {
                throw (ex);
                postdata = null;
            }
            return listpostdatamedia;
        }

        public async Task<List<NotificID>> PostComment(PostComment _postComment)
        {
            List<NotificID> _pstComment = new List<NotificID>();
            try
            {
                SqlParameter _postNo = new SqlParameter("@PostNo", _postComment.PostNo);
                SqlParameter _senderId = new SqlParameter("@SenderId", _postComment.SenderId);
                SqlParameter _receiverId = new SqlParameter("@ReceiverId", _postComment.ReceiverId);
                SqlParameter _comment = new SqlParameter("@Comment", _postComment.Comment);
                SqlParameter _companyId = new SqlParameter("@CompanyId", _postComment.CompanyId);
                DBContext db = new DBContext();
                _pstComment = await db.Set<NotificID>().FromSqlRaw("Proc_PostComment @PostNo, @SenderId, @ReceiverId, @Comment, @CompanyId", _postNo, _senderId, _receiverId, _comment, _companyId).ToListAsync();
            }
            catch (Exception ex)
            {
                throw (ex);
                _pstComment = null;
            }
            return _pstComment;
        }

        public async Task<List<CommentCount>> CommentCountByUserId(CommentPostNo _postNo)
        {
            List<CommentCount> _cmtCount = new List<CommentCount>();
            try
            {
                SqlParameter _post = new SqlParameter("@PostNo", _postNo.PostNo);
                DBContext db = new DBContext();
                _cmtCount = await db.Set<CommentCount>().FromSqlRaw("Proc_PostCommentCount @PostNo", _post).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
                _cmtCount = null;
            }
            return _cmtCount;
        }

        /// <summary>
        /// api/MyPost/PostcommentList
        /// </summary>
        /// <param name="__postCmt"></param>
        /// <returns></returns>
        public async Task<List<PostCommenText>> CommentedText(PostCommentList __postCmt)
        {
            List<PostCommenText> _cmtList = new List<PostCommenText>();
            try
            {
                SqlParameter _post = new SqlParameter("@PostNo", __postCmt.PostNo);
                SqlParameter _userName = new SqlParameter("@UserName", __postCmt.UserName);
                DBContext db = new DBContext();
                _cmtList = await db.Set<PostCommenText>().FromSqlRaw("Proc_PostCommentedList @PostNo, @UserName", _post, _userName).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
                _cmtList = null;
            }
            return _cmtList;
        }

        public async Task<List<PostLocationDto>> ReplyComment(PostCommenText postCommenText)
        {
            List<PostLocationDto> _replyComment = new List<PostLocationDto>();
            try
            {
                SqlParameter _postNo = new SqlParameter("@PostNo", postCommenText.PostNo);
                SqlParameter _senderId = new SqlParameter("@SenderId", postCommenText.ReceiverUser);
                SqlParameter _receiverId = new SqlParameter("@ReceiverId", postCommenText.SenderUser);
                SqlParameter _comment = new SqlParameter("@Comment", postCommenText.TextMsg);
                DBContext db = new DBContext();
                _replyComment = await db.Set<PostLocationDto>().FromSqlRaw("Proc_ReplyPostComment @PostNo, @SenderId, @ReceiverId, @Comment", _postNo, _senderId, _receiverId, _comment).ToListAsync();
            }
            catch (Exception ex)
            {
                throw (ex);
                _replyComment = null;
            }
            return _replyComment;
        }

        public async Task<List<PostCommentList>> GetCommentedUser(CommentPostNo _postNo)
        {
            List<PostCommentList> obj = new List<PostCommentList>();
            try
            {
                SqlParameter postNo = new SqlParameter("@PostNo", _postNo.PostNo);
                DBContext _context = new DBContext();
                obj = await
                _context.Set<PostCommentList>().FromSqlRaw("SProc_Sales_GetCommentedUserListe @PostNo", postNo).ToListAsync();
            }
            catch (Exception ex)
            {
                throw (ex);
                obj = null;
            }
            return obj;
        }

        public async Task<List<SaveCammentCount>> GetSaveCammentCount(PostCommentList _postCommentList)
        {
            List<SaveCammentCount> obj = new List<SaveCammentCount>();
            try
            {
                SqlParameter _post = new SqlParameter("@PostNo", _postCommentList.PostNo);
                SqlParameter _userName = new SqlParameter("@UserName", _postCommentList.UserName);
                DBContext _context = new DBContext();
                obj = await
                _context.Set<SaveCammentCount>().FromSqlRaw("SProc_Sales_GetSaveCommentedCntNo @PostNo,@UserName", _post, _userName).ToListAsync();
            }
            catch (Exception ex)
            {
                throw (ex);
                obj = null;
            }
            return obj;
        }//
    }
}