﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sales.Repositories.Helper
{
    public class ApiConstant
    {
        public static string SP_UpdateIsfavourite = "SProc_Sales_MakePostFavourite";
        public static string SPocUpdateMyProfile = "sp_UpdateMyProfile";
        public static string GetLoginUserProfile = "SProc_GetLoginUserProfile";
        public static string GetUserMyProfile = "sp_GetUserMyProfile";
    }
}
