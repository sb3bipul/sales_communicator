﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Sales.Repositories.Helper
{
    public class DBConnection
    {
        private SqlConnection _con;
        private string apiConnectionAuthString;
        public DBConnection(string connectionString)
        {
            apiConnectionAuthString = connectionString;
        }

        public SqlConnection GetApiConnection()
        {
            _con = new SqlConnection(apiConnectionAuthString);
            return this._con;

        }

        /// <summary>
        /// Open SQL Connection to connect to Database.
        /// </summary>
        public void ConnectionOpen()
        {
            if (_con.State != ConnectionState.Open)
            {
                _con.Close();
                _con.Open();
            }

        }

        /// <summary>
        /// Close SQL Connection from the Database.
        /// </summary>
        public void ConnectionClose()
        {
            if (_con.State == ConnectionState.Open)
            {
                _con.Close();
            }
        }
    }
}
