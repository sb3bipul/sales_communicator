import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, interval, Subscription } from 'rxjs';
import { LoginService } from '../_services/login.service';
import { NotificationService } from '../_services/notification.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  //private loggedIn = new BehaviorSubject<boolean>(false);
  userRole: string;
  nCount: string;
  notificationCountNo = [];
  setInterval = setInterval;
  private updateSubscription: Subscription;
  userName:string;
  constructor(private router: Router, private authService: LoginService, private mynote: NotificationService) { }

  ngOnInit() {
    this.userRole = sessionStorage.getItem("roleId");
    this.userName = sessionStorage.getItem("UserName");
    console.log('NotificationUserName',this.userName);
    //this.getNotificationCount(); 
    this.updateSubscription = interval(1000).subscribe(
      (val) => {
         this.getNotificationCount()
      }
   );  
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }

  getNotificationCount() {
      this.mynote.getNotificationCount(this.userName).subscribe(
      (result: any) => {
        this.notificationCountNo = result["Payload"];
        // console.log("NotificationCount",this.notificationCountNo);        
      }
    );
  }
}
