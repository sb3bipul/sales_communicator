import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards/index';
import { LoginComponent } from './login/login.component';
const appRoutes: Routes = [
    //{ path: 'invoice', component: MyQueueComponent },
    { path: 'login', component: LoginComponent },

    { path: '**', redirectTo: '' }
];
    
export const routing = RouterModule.forRoot(appRoutes);