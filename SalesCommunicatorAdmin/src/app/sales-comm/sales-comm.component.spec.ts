import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesCommComponent } from './sales-comm.component';

describe('SalesCommComponent', () => {
  let component: SalesCommComponent;
  let fixture: ComponentFixture<SalesCommComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesCommComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesCommComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
