import { Component, OnInit,TemplateRef, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { MyPostService } from '../_services/my-post.service';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { Gallery, GalleryItem, ThumbnailsPosition, ImageSize } from "ng-gallery";
import { Lightbox } from "ng-gallery/lightbox";
import { post } from 'jquery';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-sales-comm',
  templateUrl: './sales-comm.component.html',
  styleUrls: ['./sales-comm.component.css']
})
export class SalesCommComponent implements OnInit {
  userPostList: string[]; privacyType: string; PostType: string; publishedDate: Date; createdBy: string;
  MediaFiles: string[]; Commands: string; enddate: string; startdate: string; resultArray: {}; postMediaList = [];
  tagName: string; myJsonString: any;
  UserRole: string;
  mType:string;
  items: GalleryItem[];
  @ViewChild("itemTemplate", { static: true }) itemTemplate: TemplateRef<any>;
  id :string;
  UserRoleID: string;
  counter: number = 0;
  showModal: boolean;
  imgUrl:string[];
  selectedindex: number = 0;
  constructor(private _UserPost: MyPostService, private toastr: ToastrService, private modalService: ModalService, public gallery: Gallery, public lightbox: Lightbox, private route:ActivatedRoute) { }

  ngOnInit(): void {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.UserRoleID = sessionStorage.getItem("UserName");
    this.mType= localStorage.getItem('menuType')
    console.log("Menu Click====>",this.mType);
    this.LoadUserPostData(this.startdate, this.enddate);
    this.id = this.route.snapshot.paramMap.get('id');
    console.log('Id'+this.id);
  }
  searchuserData() {
    this.LoadUserPostData(this.startdate, this.enddate);
  }

  private LoadUserPostData(FromDate: string, ToDate: string) {
    //alert(this.PostType);
    this.UserRole = sessionStorage.getItem("User");
    var _fromDt = null;
    var _toDt = null;
    var _Privacy = null;
    
    var _CreatedBy = null;
    var _tagNam = null;
    var _UserName = null;
    if (this.tagName == null || this.tagName == undefined) {
      _tagNam = null;
    }
    else {
      _tagNam = this.tagName;
    }

    if (sessionStorage.getItem("UserName") != null || sessionStorage.getItem("UserName") != undefined) {
      _UserName = sessionStorage.getItem("UserName");
    }
    this._UserPost.registerUsers(_CreatedBy, _Privacy, this.mType, FromDate, ToDate, _tagNam, _UserName, this.UserRole).subscribe(
      _UserPostDt => {
        this.userPostList = JSON.parse(JSON.stringify(_UserPostDt)).Payload;
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }
  makepostFav(postNo) {
    var PostNo = String(postNo);
    var UserName = this.UserRoleID;
    this._UserPost.MakePostFav(PostNo,UserName).subscribe(
      _UserPostDt => {
        this.toastr.success("You love this post", "success");
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }

  reset() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
    this.PostType = '';
    this.tagName = '';
  }

  onNext() {
    if (this.counter != this.imgUrl.length - 1) {
      this.counter++;
    }
  }

  onPrevious() {
    if (this.counter > 0) {
      this.counter--;
    }
  }
  
  hide()
  {
    this.showModal = false;
    this.imgUrl=[];
  }
  openImg1(i:any,j:any){    
    this.counter=i;
    this.imgUrl=j;
    this.showModal=true;
  }
}
