import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { MyPostService } from '../_services/my-post.service';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { Gallery, GalleryItem, ThumbnailsPosition, ImageSize } from "ng-gallery";
import { Lightbox } from "ng-gallery/lightbox";
import { post } from 'jquery';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { trigger, transition, query, style, animate, group} from '@angular/animations';
import { delay } from 'rxjs/operators';

const left = [
  query(':enter, :leave', style({ position: 'fixed', width: '200px' }), {
    optional: true,
  }),
  group([
    query(
      ':enter',
      [
        style({ transform: 'translateX(-200px)' }),
        animate('.3s ease-out', style({ transform: 'translateX(0%)' })),
      ],
      {
        optional: true,
      }
    ),
    query(
      ':leave',
      [
        style({ transform: 'translateX(0%)' }),
        animate('.3s ease-out', style({ transform: 'translateX(200px)' })),
      ],
      {
        optional: true,
      }
    ),
  ]),
];

const right = [
  query(':enter, :leave', style({ position: 'fixed', width: '200px' }), {
    optional: true,
  }),
  group([
    query(
      ':enter',
      [
        style({ transform: 'translateX(200px)' }),
        animate('.3s ease-out', style({ transform: 'translateX(0%)' })),
      ],
      {
        optional: true,
      }
    ),
    query(
      ':leave',
      [
        style({ transform: 'translateX(0%)' }),
        animate('.3s ease-out', style({ transform: 'translateX(-200px)' })),
      ],
      {
        optional: true,
      }
    ),
  ]),
];

@Component({
  selector: 'app-userpost',
  templateUrl: './userpost.component.html',
  styleUrls: ['./userpost.component.css'],
  animations: [
    trigger('animImageSlider', [
      transition(':increment', right),
      transition(':decrement', left),
    ]),
  ],
})
export class UserpostComponent implements OnInit {
  public loading: boolean = false;
  isHidden: boolean = true;
  public hideRuleContent: boolean[] = [];
  userPostList: string[]; privacyType: string; PostType: string; publishedDate: Date; createdBy: string;
  MediaFiles: string[]; Commands: string; enddate: string; startdate: string; resultArray: {}; postMediaList = [];
  tagName: string; myJsonString: any; click: boolean = false;
  UserRole: string;  UserRoleID: string;  items: GalleryItem[];
  commentText: string; replyText:string;commentUser: string; postCommentList: string[];replycommenttoUser:string;
  @ViewChild("itemTemplate", { static: true }) itemTemplate: TemplateRef<any>;
  closeResult: string;  displayStyle = "none";
  createElement: boolean=false;  PostNo:string;cmtPostNo:string;
  SenderUser:string; ReceiverUser:string;postCommentUserList:string;
  CommentedUserListPostNo:string;  CommentedUserListUserName:string;
  SaveCommentPostNo:string; SaveCommentCountResponse:string[];
  sandy:string;name:string;

  showModal: boolean;
  currImgUrl:string;
  imgUrl:string[];
  counter: number = 0;
  selectedindex: number = 0;
  constructor(private modalServices: NgbModal, private _UserPost: MyPostService, private toastr: ToastrService, private modalService: ModalService, public gallery: Gallery, public lightbox: Lightbox) { }

  ngOnInit() {
    this.UserRoleID = sessionStorage.getItem("UserName");
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
    this.sandy='0';
    console.log("user role id",sessionStorage.getItem("roleId"));
  }

  openImg1(i:any,j:any){    
    this.counter=i;
    this.imgUrl=j;
    this.showModal=true;
  }

  searchuserData() {
    this.LoadUserPostData(this.startdate, this.enddate);
  }

  private LoadUserPostData(FromDate: string, ToDate: string) {
    this.loading = true;
    //alert(this.PostType);
    this.UserRole = sessionStorage.getItem("User");
    var _fromDt = null;
    var _toDt = null;
    var _Privacy = null;
    var _posttype = '';
    var _CreatedBy = null;
    var _tagNam = null;
    var _UserName = null;
    if (this.PostType == null || this.PostType == undefined) {
      _posttype = '';
    }
    else {
      _posttype = this.PostType;
    }
    if (this.tagName == null || this.tagName == undefined) {
      _tagNam = null;
    }
    else {
      _tagNam = this.tagName;
    }

    if (sessionStorage.getItem("UserName") != null || sessionStorage.getItem("UserName") != undefined) {
      _UserName = sessionStorage.getItem("UserName");
    }
    this._UserPost.registerUsers(_CreatedBy, _Privacy, _posttype, FromDate, ToDate, _tagNam, _UserName, this.UserRole).subscribe(
      _UserPostDt => {
        this.userPostList = JSON.parse(JSON.stringify(_UserPostDt)).Payload;
        console.log("userpostlist", this.userPostList);
        this.loading = false;
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }
  onCommentChange(commnetValue: string): void {
    this.commentText=commnetValue;
    console.log("Texted value====>",this.commentText);
  } 

  Getcommentedhistory(event:any){
    this._UserPost.CommentListByUser(event.PostNo, event.UserName).subscribe(_postCommList => {
      this.postCommentList = _postCommList["Payload"];
      console.log("commentedlist", this.postCommentList);
      //this.openPopup1();   
    })
    this.PostNo=event.PostNo;
    this.SenderUser="scadmin";
    this.ReceiverUser=event.UserName;
    this.createElement=true;
  }
  reload(){
    this._UserPost.CommentListByUser(this.PostNo, this.ReceiverUser).subscribe(_postCommList => {
      this.postCommentList = _postCommList["Payload"];
    })
  }
  add(event:any){
    this.openPopup();
    console.log("Reply comment==>",event.PostNo,event.UserName)    
  }

  SaveComment(event: any) {
    var _compId = "2";
    // console.log("comm==>",event.POSTNO,event.CREATEDBY,this.commentText,_compId,this.UserRoleID);    
    this._UserPost.AddComment(event.POSTNO, this.UserRoleID, event.CREATEDBY, this.commentText, _compId).subscribe(_postComm => {
      this.commentUser = _postComm["Payload"];
      this.SaveCommentPostNo=event.POSTNO;
      this.clear();
      delay(1000);
      this.CommentUpdate();
    })
  }

  CommentUpdate(){
    this._UserPost.SaveCommentCount(this.SaveCommentPostNo, this.UserRoleID).subscribe(_postCommCountRes => {
    this.SaveCommentCountResponse=_postCommCountRes["Payload"];
    console.log("SaveCommentCountResponse===>",this.SaveCommentCountResponse);
    this.sandy=this.SaveCommentCountResponse[0]["savecommentCountNo"];
    this.clear();
    console.log("hhhhhhhhhhhhhhhhhhhh=====>>>>>>",this.sandy);
  })
  }

  SaveReply(){
    console.log(this.PostNo,this.ReceiverUser,this.SenderUser,this.commentText);
    this._UserPost.ReplyComment(this.PostNo,this.ReceiverUser,this.SenderUser,this.commentText).subscribe(_postComm => {
      this.replycommenttoUser = _postComm["Payload"];
      this.SaveCommentPostNo=this.PostNo;
      this.UserRoleID=this.SenderUser;
      this.CommentUpdate();
      this.reload();
      this.name='';
    })
  }

  openImg(i:any){    
    this.currImgUrl=i;
    this.showModal=true;
  }

  hide()
  {
    this.showModal = false;
    this.imgUrl=[];
  }

  makepostFav(postNo) {
    var PostNo = String(postNo);
    var UserName = this.UserRoleID;
    this._UserPost.MakePostFav(PostNo, UserName).subscribe(
      _UserPostDt => {
        this.click = true;
        this.toastr.success("You love this post", "success");
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }

  clear() {
    this.commentText = '';
    this.replyText='';
    console.log("text empty");
  }

  reset() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
    this.PostType = '';
    this.tagName = '';
  }

  toggle(index) {
    console.log("oooooooooooooo", index)
    this.hideRuleContent[index] = !this.hideRuleContent[index];
  }
  //For User Login
  CommentList(PostNo:string) {
    //this.cmtPostNo=event.POSTNO;
    //console.log("cmtPostNo",this.cmtPostNo);
    this.postCommentList=[];
    this._UserPost.CommentListByUser(PostNo, this.UserRoleID).subscribe(_postCommList => {
      this.postCommentList = _postCommList["Payload"];
      console.log("commentedlist", this.postCommentList);
      this.openPopup();
    })
  }
  //For Admin Login
  CommentedUserList(PostNo:string)
  {
    this._UserPost.CommentedListUser(PostNo).subscribe(_postCommList => {
      this.postCommentUserList = _postCommList["Payload"];
      console.log("commentedlist", this.postCommentUserList);
      this.PostNo = this.postCommentUserList[0]["PostNo"];
      console.log("commentedlistPostNo", this.PostNo);
      this.ReceiverUser= this.postCommentUserList[0]["UserName"];
      console.log("commentedlistReceiverUser", this.ReceiverUser);
      if(this.PostNo!=null||this.PostNo!=undefined)
      {
        this._UserPost.CommentListByUser(this.PostNo, this.ReceiverUser).subscribe(_postCommList => {
          this.postCommentList = _postCommList["Payload"];
          this.createElement=true;
          console.log("commentedlistByDefault", this.postCommentList);
        })
      }
    this.openPopup();
    this.SenderUser="scadmin";
    this.clear();
    })
  }

  open(content) {
    this.modalServices.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  openPopup() {
    this.displayStyle = "block";
  }
  closePopup() {
    this.displayStyle = "none";
    this.postCommentList=[];
    this.createElement=false;
  }

  onNext() {
    if (this.counter != this.imgUrl.length - 1) {
      this.counter++;
    }
  }

  onPrevious() {
    if (this.counter > 0) {
      this.counter--;
    }
  }
}