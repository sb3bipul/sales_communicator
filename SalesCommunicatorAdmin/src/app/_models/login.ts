export class ILogin {
  Domain: string;
  Event: string;
  ClientData: string;
  Status: string;
  Payload: string;
}

export class Login {
  Authorization: string;
  access_token: string;
  token_type: string;
  expires_in: string;
  userName: string;
  userFullName: string;
  companyId: string;
  companyName: string;
  role: string;
  email: string;
  phone: string;
  BrokerPicUri: string;
  IsNewReleaseFlag: string;
  UserType: string;
  LoginCompanyId: string;
}

export class User {
  UserName: string;
  Password: string;
  isLoggedIn: boolean;
} 