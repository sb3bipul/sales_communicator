export enum Role {
    Broker = 'Broker01',
    Admin = 'Admin',
    Customer = 'Customer',
    SalesMgr = 'SalesManager'
}
