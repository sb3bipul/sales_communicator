import { Role } from './role';

export class SCUser {
  role: Role;
}


export class IInvoice {
    id: string;
    sign_image: string;
    customer_id: string;
    driver_id: string;
    invoice_number: string;
    invoice_url: string;
    customer_name: string;
    latitude: string;
    company_id: string;
    invoice_comment: string;
    longitude: string;
    status: string;
    created_at: string;
    updated_at: string;
    image_url: string;
    signatory_name: string;
    driver_name: string;
}
export class IInvoiceApproval {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class PImage {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}
export class PDashbordData {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    DailyCollection: string;
    TotalCollection: string;
    MonthlyCollection: string;
    TotalCP: string;
    TotalAssignedCust: string;
}
export class DailyCollection {
    DailyCollection: string;
}

export class TotalCollection {
    TotalCollection: string;
}

export class MonthlyCollection {
    MonthlyCollection: string;
}

export class TotalCP {
    TotalCP: string;
}

export class TotalAssignedCust {
    TotalAssignedCust: string;
}

export class IInvoicePdfStatus {
    InvoicePDFUrl: string;
    Status: string;
}
export class IInvoiceAddExtra {
    invoiceId: string;
}

export class InvoiceStatusLog {
    //id:string;
    invoice_id: string;
    status: string;
    created_at: string;
    updated_at: string;
}

export class User {
    UserId: string;
    Password: string;
    CompanyId: number;
    Email: string | null;
    PhoneNumber: string | null;
    RoleId: string;
    UserName: string;
    CompanyName: string;
    CreateDate: Date | null;
    UpdatedDate: Date | null;
    CreatedBy: string | null;
    UpdatedBy: string | null;
    RoleName: string;
}

export class UserRole {
    RoleId: number;
    CompanyId: number;
    Name: string
    Description: string;
    IsActive: boolean;
}

export class RmvPayment {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}