export class MyPost {
    CreatedBy: string;
    PrivacyTypeId: number;
    PostTypeId: number;
    Commends: string;
    FromDate: Date;
    ToDate: Date;
    Header: string;
    CompanyId:string;
    DivisionId:string;
    GroupId:string;
}

export class MyPostData {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}


export class PostMedia {
    PostNo: string;
    MediaName: string;
    MediaBase64: string;
    ct: string;
}

export class UserPostMedia {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class CompanyList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class BrokerGroup {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class Division {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}

export class ZoneList {
    Domain: string;
    Event: string;
    ClientData: string;
    Status: string;
    Payload: string;
}