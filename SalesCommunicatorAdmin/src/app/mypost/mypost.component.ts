import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import 'rxjs/add/operator/map';
import { MyPostService } from '../_services/my-post.service';
import { ToastrService } from 'ngx-toastr';
//import { stringify } from '@angular/compiler/src/util';
import { NgxSpinnerService } from 'ngx-spinner';
//import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/filter';
import { delay } from 'rxjs/operators';
import { FileUpload } from '..//_models/file-upload';

@Component({
  selector: 'app-mypost',
  templateUrl: './mypost.component.html',
  styleUrls: ['./mypost.component.css']
})
export class MypostComponent implements OnInit {
  public loading: boolean = false;
  mediaBase64: string; postmedia: string; BaseImageString: any;
  PrivacyType: number; PostType: number; commands: string; FromDate: Date; ToDate: Date; header: string;
  urls = []; CreatedBy: string; PostNoList: string[]; postNo: string; MediaPolist: string[]; MediaPO: string;
  BaseMediaString = []; companyData = []; CompanyCode: string; division: string; zone: string; group: string;
  CompanyListData = []; ZoneData = []; ZoneListData = []; tagItems: string;
  GroupData = []; GroupListData = []; DivisionData = []; DivisionListData = [];
  userMediadata: any; UserName: string; tag = []; UserRole: string;
  IsMediaPost: any; isValidDate: any; excel: string; pdf: string; ppt: string; ExcelBase64: string; PdfBase64: string; PPTBase64: string;
  UserList:string[];
  @Input() files: FileUpload[];
  @Input() allowNull: boolean;

  constructor(private formbulider: FormBuilder, private http: HttpClient, private router: Router, private myPost: MyPostService, private toastr: ToastrService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {
    this.PrivacyType = 1;
    this.PostType = 1;
    this.UserName = sessionStorage.getItem("FullName");

    this.loadCompanyInfoData();
    this.GetAllZone();
    
  }

  onSelectFile(event) {
    debugger
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      for (let i = 0; i < filesAmount; i++) {
        var reader = new FileReader();
        reader.onload = (event: any) => {
          this.urls.push(event.target.result);
          let reader = event.target;
          var base64result = reader.result.substr(reader.result.indexOf(',') + 1);
          this.BaseMediaString.push(base64result);
        }
        reader.readAsDataURL(event.target.files[i]);
      }
    }
  }

  onSelectExcelFile(event) {
    debugger;
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      //this.urls.push(event.target.result);
      let reader = event.target;
      var excel64 = reader.result.substr(reader.result.indexOf(',') + 1);
      this.ExcelBase64 = String(excel64);
    }
  }

  onSelectPdfFile(event) {
    debugger;
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = (event: any) => {
      //this.urls.push(event.target.result);
      let reader = event.target;
      var base64pdf = reader.result.substr(reader.result.indexOf(',') + 1);
      this.PdfBase64 = String(base64pdf);
    }
    reader.readAsDataURL(file);
    console.log("pdf 64 " + this.PdfBase64);
  }

  onSelectPPTFile(event) {
    debugger;
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event: any) => {
      this.urls.push(event.target.result);
      let reader = event.target;
      var pptBase64 = reader.result.substr(reader.result.indexOf(',') + 1);
      this.PPTBase64 = String(pptBase64);
    }
    console.log("PPT base 64 " + this.PPTBase64);
  }

  onKeypress(event) {
    this.commands = this.commands + "\\n";
  }

  SaveUserPost() {
    this.loading = true;
    this.SpinnerService.show();
    // this.CreatedBy = sessionStorage.getItem("FullName"); //Commented on 01/12/2021
    this.CreatedBy = sessionStorage.getItem("UserName");
    var _Company = '';
    var _Division = '';
    var _Group = '';
    var strTag = '';

    this.isValidDate = this.validateDates(this.FromDate, this.ToDate);

    if (this.isValidDate) {
      if (this.CompanyCode == null || this.CompanyCode == undefined) {
        _Company = '';
      }
      else {
        _Company = this.CompanyCode;
      }

      if (this.division == null || this.division == undefined) {
        _Division = '';
      }
      else {
        _Division = this.division;
      }
      if (this.group == null || this.group == undefined) {
        _Group = '';
      }
      else {
        _Group = this.group;
      }
      if (this.tagItems == null || this.tagItems == undefined) {
        strTag = '';
      }
      else {
        strTag = this.tagItems;
      }
      
      this.myPost.insertUserPostData(this.CreatedBy, Number(this.PrivacyType), Number(this.PostType), this.commands, this.FromDate, this.ToDate, this.header, _Company, _Division, _Group ,this.zone).subscribe(
        _saveUserPost => {
          this.PostNoList = JSON.parse(JSON.stringify(_saveUserPost)).Payload;
          this.postNo = this.PostNoList[0]["POSTNO"];
          // this.SpinnerService.show();
          this.IsMediaPost = this.InsertMediaData(this.postNo);
          // delay(40000);
          if (this.IsMediaPost) {
            if (this.postNo != null || this.postNo != undefined) {
              if (strTag != null || strTag != undefined) {
                for (var i = 0; i < strTag.length; i++) {
                  this.myPost.InsertUserPostTag(this.postNo, strTag[i], '').subscribe(
                    _tagData => { }
                  );
                }
              }
              this.myPost.InsertUserPostLocation(this.postNo, this.zone).subscribe(
                _tahLoc => { }
              );
              this.myPost.GetUserListByZoneGroupDiv(this.zone,_Group,_Division).subscribe(
                _getUserList=>{
                  this.UserList=JSON.parse(JSON.stringify(_getUserList)).Payload;
                }
              )
            }
            // this.SpinnerService.hide();
            // this.toastr.success("Media successfully posted.", "Success");
            // this.loading = false;
            this.reset();
          }

          this.SpinnerService.hide();
          this.loading = false;
          this.toastr.success("Media successfully posted.", "Success");
        },
        error => {
          console.log('Failed post user data. ' + error);
          this.toastr.error("something went wrong please try again.", "Error");
        });
    }
    //alert(this.MediaPO);
  }

  cancel() {
    this.reset();
    this.router.navigate(['./userpost']);
  }

  reset() {
    this.commands = null;
    this.postmedia = null;
    this.FromDate = null;
    this.ToDate = null;
    this.BaseMediaString = null;
    this.mediaBase64 = null;
    this.BaseImageString = null;
    this.urls = null;
    this.header = null;
    this.CompanyCode = null;
    this.division = '';
    this.group = '';
    this.zone = '';
    this.tagItems = null;
    //sessionStorage.clear();
  }

  loadCompanyInfoData() {
    this.SpinnerService.show();
    var _companyCode = '';
    this.myPost.CompanyList(_companyCode).subscribe(
      (result: any) => {
        this.companyData = result["Payload"];
        for (var i = 0; i < this.companyData.length; i++) {
          this.CompanyListData.push(this.companyData[i]);
        }
        console.log(this.CompanyListData);
        this.SpinnerService.hide();
      }
    );
  }

  onChangeLoadDivision(CompanyId: string) {
    this.SpinnerService.show();
    var _division = '';
    this.myPost.getDivisionByCompany(CompanyId, _division).subscribe(
      (result: any) => {
        this.DivisionData = result["Payload"];
        for (var i = 0; i < this.DivisionData.length; i++) {
          this.DivisionListData.push(this.DivisionData[i]);
        }
        console.log(this.DivisionListData);
        this.SpinnerService.hide();
      }
    );
  }

  GetAllZone() {
    this.SpinnerService.show();
    var _CompanyId = '';
    if (this.CompanyCode == '' || this.CompanyCode == undefined) {
      _CompanyId = '';
    } else {
      _CompanyId = this.CompanyCode;
    }
    this.myPost.getAllZone(_CompanyId).subscribe(
      (result: any) => {
        this.ZoneData = result["Payload"];
        for (var i = 0; i < this.ZoneData.length; i++) {
          this.ZoneListData.push(this.ZoneData[i]);
        }
        console.log(this.ZoneListData);
        this.SpinnerService.hide();
      }
    );
  }

  onChangedDivision(_DivisionId: string) {
    var _CompanyId = '';
    var _groupId = '';
    if (this.CompanyCode == '' || this.CompanyCode == undefined) {
      _CompanyId = '';
    }
    else {
      _CompanyId = this.CompanyCode;
    }
    this.myPost.getGroupListByCompany(_CompanyId, _DivisionId, _groupId).subscribe(
      (result: any) => {
        this.GroupData = result["Payload"];
        for (var i = 0; i < this.GroupData.length; i++) {
          this.GroupListData.push(this.GroupData[i]);
        }
        console.log(this.GroupListData);
        this.SpinnerService.hide();
      }
    );
  }

  validateDates(sDate: Date, eDate: Date) {
    this.isValidDate = true;
    if ((sDate == undefined || sDate == null) || (eDate == undefined || eDate == null)) {
      this.toastr.warning("Please select post duration", "Warning");
      this.isValidDate = false;
    }

    if ((sDate != undefined || sDate != null) && (eDate != undefined || eDate != null) && (eDate) < (sDate)) {
      this.toastr.warning("To date should be grater then from date.", "Warning");
      this.isValidDate = false;
    }
    return this.isValidDate;
  }

  public options = {
    readonly: undefined,
    placeholder: '+ Tag'
  };

  public onAdd(item) {
    console.log('tag added: value is ' + item);
  }

  public onRemove(item) {
    console.log('tag removed: value is ' + item);
  }

  public onSelect(item) {
    console.log('tag selected: value is ' + item);
  }

  public onFocus(item) {
    console.log('input focused: current value is ' + item);
  }

  public onTextChange(text) {
    console.log('text changed: value is ' + text);
  }

  public onBlur(item) {
    console.log('input blurred: current value is ' + item);
  }

  public onTagEdited(item) {
    console.log('tag edited: current value is ' + item);
  }

  public onValidationError(item) {
    console.log('invalid tag ' + item);
  }


  public InsertMediaData(PostNo: string) {
    //this.IsMediaPost = true;
    if (this.BaseMediaString.length > 0) {
      for (let i = 0; i < this.BaseMediaString.length; i++) {
        this.myPost.insertUserPostMediaData(PostNo, 'abc.jpeg', this.BaseMediaString[i], this.PdfBase64, 'abc.pdf', 'abc.xlsx', this.ExcelBase64, 'abc.pptx', this.PPTBase64, String(i)).subscribe(
          _mediaData => {
            this.MediaPolist = JSON.parse(JSON.stringify(_mediaData)).Payload;
            this.MediaPO = this.MediaPolist[i]["POSTNO"];
          },
          error => {
            console.log('Failed post user data. ' + error);
            this.toastr.error("something went wrong please try again.", "Error");
          });
      }
      this.IsMediaPost = true;
    }
    else {
      this.toastr.warning("Post media not selected or something went wrong please try again. ", "Warning");
      this.IsMediaPost = false;
    }
    return this.IsMediaPost;
  }

  //---------File upload-----------//
  onFileSelect(event) {
    let f = event.target.files[0];
    let newFile: FileUpload = {
      fileId: 1,
      fileName: f.name
    }
    if (!this.files) {
      this.files = new Array();
    }
    this.files.push(newFile);
  }
}
