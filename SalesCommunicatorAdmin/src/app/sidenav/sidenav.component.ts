import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';
import { Role } from '../_models/role';
import { LoginService } from '../_services/login.service';
import { filter, pairwise } from 'rxjs/operators'

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {
  userRole: string;
  checked: boolean = false;
  currentRoute: string; PostType: string;
  returnUrl:string;

  constructor(private router: Router, private authService: LoginService) {
    router.events
      .pipe(filter(e => e instanceof NavigationEnd))
      .subscribe((e: NavigationEnd) => {
        this.currentRoute = e.url;
      });
  }
// redirect()
// {
//   this.router.navigate(['/salescontest'],{skipLocationChange:true})
// }

  ngOnInit() {
    this.userRole = sessionStorage.getItem("roleId");
  }
  get isAuthorized() {
    return this.authService.isAuthorized();
  }

  get isAdmin() {
    return this.authService.hasRole(Role.Admin);
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['login']);
  }
  selectedevent(event : string) {
    localStorage.setItem('menuType',event) 
     console.log(event);    
     this.returnUrl = '/salescomm';
     this.redirectTo('/salescomm/') 
  }
  redirectTo(uri:string)
  {
    this.router.navigateByUrl('/',{skipLocationChange:true}).then(()=>
    this.router.navigate([uri]));
  }
}
