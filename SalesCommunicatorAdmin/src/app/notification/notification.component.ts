import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { interval, Subscription } from 'rxjs';
//import { delay } from 'rxjs-compat/operator/delay';
import { delay } from 'rxjs/operators';
import { MyPostService } from '../_services/my-post.service';
import { NotificationService } from '../_services/notification.service'

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit {
  selectedindex: number = 0;
  public loading:boolean=false;
  counter: number = 0;
  showModal: boolean;
  imgUrl:string[];
  currImgUrl:string;
  
  userRole: string;
  logedInRole: string;
  countNo = []; countData = [];
  notfyData = [];
  notfyListData = []; userResult = [];
  click: boolean = false;
  private updateSubscriptionn: Subscription;
  constructor(private myNotification: NotificationService, private _UserPost: MyPostService, private toastr: ToastrService) { }

  ngOnInit(): void {
    this.logedInRole = sessionStorage.getItem("roleId");//commented on 15/11/2021
    this.userRole = sessionStorage.getItem("UserName");
    console.log("userRole", this.userRole);
    console.log("logedInRole", this.logedInRole);
    this.loadNotificationList();
  }
  loadNotificationList() {
    this._UserPost.GetUserPostDataNotification(this.userRole).subscribe(
      (result: any) => {
        this.notfyData = result["Payload"];
        for (var i = 0; i < this.notfyData.length; i++) {
          this.notfyListData.push(this.notfyData[i]);
        }
        this.updateNotificationStatus();
        console.log('NotificationList', this.notfyListData);
      }
    );
  }
  makepostFav(postNo) {
    var PostNo = String(postNo);
    var _userId = this.userRole;
    this._UserPost.MakePostFav(PostNo, _userId).subscribe(
      _UserPostDt => {
        this.click = true;
        this.toastr.success("You love this post", "success");
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }
  updateNotificationStatus() {
    //delay(40000);
    this.myNotification.UpdateNotification(this.userRole).subscribe(
      (result: any) => {
        this.userResult = result["Payload"];
        console.log('updatedUserID', this.userResult);
        
      }
    )
  }

  onNext() {
    if (this.counter != this.imgUrl.length - 1) {
      this.counter++;
    }
  }

  onPrevious() {
    if (this.counter > 0) {
      this.counter--;
    }
  }
  
  hide()
  {
    this.showModal = false;
    this.imgUrl=[];
  }
  openImg1(i:any,j:any){    
    this.counter=i;
    this.imgUrl=j;
    this.showModal=true;
  }
  openImg(i:any){    
    this.currImgUrl=i;
    this.showModal=true;
  }
}