﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { from } from 'rxjs';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { MypostComponent } from './mypost/mypost.component';
import { UserpostComponent } from './userpost/userpost.component';
import { AppRoutingGuard } from './app-routing.guard';
import { LoginService } from './_services/login.service';
import { Role } from './_models/role';
import { FavouriteComponent } from './favourite/favourite.component';
import { PromotionComponent } from './promotion/promotion.component';;
import { SalesContestComponent } from './sales-contest/sales-contest.component';
import { NewProductComponent } from './new-product/new-product.component';
import { PriceChangeComponent } from './price-change/price-change.component';
import { SuperMarketComponent } from './super-market/super-market.component';
import { NotificationComponent } from './notification/notification.component';
import { SalesCommComponent } from './sales-comm/sales-comm.component';

const routes: Routes = [
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      { path: '', component: LoginComponent, pathMatch: 'full' },
      { path: 'login', component: LoginComponent, pathMatch: 'full' }
    ]
  },
  {
    path: '',
    component: HomeLayoutComponent,
    children: [
      {
        path: 'myPost', component: MypostComponent, canActivate: [AppRoutingGuard],
        data: {
          roles: [
            Role.Admin,
          ]
        },
      },
      { path: 'userpost', component: UserpostComponent, canActivate: [AppRoutingGuard] },
      { path: 'favourite', component: FavouriteComponent, canActivate: [AppRoutingGuard] },
      { path: 'promotion', component: PromotionComponent, canActivate: [AppRoutingGuard] },
      { path: 'salescomm/:id', component: SalesCommComponent, canActivate: [AppRoutingGuard] },
      { path: 'salescontest', component: SalesContestComponent, canActivate: [AppRoutingGuard] },
      { path: 'newProduct', component: NewProductComponent, canActivate: [AppRoutingGuard] },
      { path: 'supermarket', component: SuperMarketComponent, canActivate: [AppRoutingGuard] },
      { path: 'priceChange', component: PriceChangeComponent, canActivate: [AppRoutingGuard] },
      { path: 'notification', component: NotificationComponent, canActivate: [AppRoutingGuard] },
    ]
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
  providers: [
    AppRoutingGuard,
    LoginService
  ]
})
export class AppRoutingModule {
  static forRoot(arg0: any): any {
    throw new Error("Method not implemented.");
  }
}
