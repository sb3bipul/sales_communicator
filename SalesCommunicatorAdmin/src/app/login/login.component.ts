import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { LoginService } from '../_services/login.service';
import { Role } from '../_models/role';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  loading = false;
  loginForm: FormGroup;
  submitted = false;
  returnUrl: string;
  Role = Role;
  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private login: LoginService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      UserName: ['', Validators.required],
      Password: ['', Validators.required],
      isLoggedIn: [true, Validators.required]
    });
  }
  get loginFormControl() { return this.loginForm.controls; }

  onSubmit(role: Role) {
    this.submitted = true;
    var returnUrl = null;
    // if (this.loginForm.invalid) {
    //   //return;
    // }
    this.loading = true;
    // if (sessionStorage.getItem('roleId') == '13' || sessionStorage.getItem('roleId') == '3') {
    if (sessionStorage.getItem('roleId') == '1') {
      returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || 'myPost';
    }
    else {
      returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || 'userpost';
    }

    this.login.login(this.loginForm.value, role)
      .pipe(first())
      .subscribe(
        () => {
          this.router.navigate([returnUrl]);
        },
        () => {
          this.loading = false;
          this.loginForm.reset();
          this.loginForm.setErrors({
            invalidLogin: true
          });
        });
  }
}
