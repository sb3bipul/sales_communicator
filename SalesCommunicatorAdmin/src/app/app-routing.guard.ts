import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Router, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './_services/login.service';
import { Role } from './_models/role';

@Injectable({
  providedIn: 'root'
})
export class AppRoutingGuard implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {
  constructor(
    private router: Router,
    private loginservice: LoginService
  ) { }

  // canActivate(
  //   next: ActivatedRouteSnapshot,
  //   state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }

  // canActivate(route: ActivatedRouteSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  //   //debugger;
  //   if (!this.loginservice.isAuthorized()) {
  //     this.router.navigate(['login']);
  //     return false;
  //   }
  //   const roles = route.data.roles as Role[];
  //   if (roles && !roles.some(r => this.loginservice.hasRole(r))) {
  //     this.router.navigate(['error', 'not-found']);
  //     return false;
  //   }
  //   return true;
  // }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //alert(localStorage.getItem('authToken'));
    if (localStorage.getItem('authToken')) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canDeactivate(
    component: unknown,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
    //debugger;
    if (!this.loginservice.isAuthorized()) {
      return false;
    }
    const roles = route.data && route.data.roles as Role[];
    if (roles && !roles.some(r => this.loginservice.hasRole(r))) {
      return false;
    }
    return true;
  }
}
