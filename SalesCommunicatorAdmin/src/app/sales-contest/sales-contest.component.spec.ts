import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesContestComponent } from './sales-contest.component';

describe('SalesContestComponent', () => {
  let component: SalesContestComponent;
  let fixture: ComponentFixture<SalesContestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesContestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesContestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
