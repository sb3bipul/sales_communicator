﻿import { routing } from './app.routing';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AuthGuard } from './_guards/index';
import { A11yModule } from '@angular/cdk/a11y';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { PortalModule } from '@angular/cdk/portal';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { CdkStepperModule } from '@angular/cdk/stepper';
import { CdkTableModule } from '@angular/cdk/table';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatChipsModule } from '@angular/material/chips';
import { MatStepperModule } from '@angular/material/stepper';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { PaymentComponent } from './Account/payment.component';
import { ChartsModule } from 'ng2-charts';
import { LoginComponent } from './login/login.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import { HeaderComponent } from './header/header.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { HttpModule } from '@angular/http';
import { DatePipe, HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AgGridModule } from 'ag-grid-angular';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from './_modal';
import { HttpClient } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';
import { OverlayModule } from '@angular/cdk/overlay';
import { MypostComponent } from './mypost/mypost.component';
import { UserpostComponent } from './userpost/userpost.component';
import { FooterComponent } from './footer/footer.component'
import { LoginService } from './_services/login.service';
import { UserRoleDirective } from './_directives/user-role.directive';
import { UserDirective } from './_directives/user.directive';
import { NgImageSliderModule } from 'ng-image-slider';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { TagInputModule } from 'ngx-chips';
import { GalleryModule } from 'ng-gallery';
import { LightboxModule } from 'ng-gallery/lightbox';
import { ImageViewerModule } from '@hallysonh/ngx-imageviewer';
import { FavouriteComponent } from './favourite/favourite.component';
import { PromotionComponent } from './promotion/promotion.component';
import { SalesContestComponent } from './sales-contest/sales-contest.component';
import { NewProductComponent } from './new-product/new-product.component';
import { PriceChangeComponent } from './price-change/price-change.component';
import { SuperMarketComponent } from './super-market/super-market.component';
import { NotificationComponent } from './notification/notification.component';
import {SalesCommComponent} from './sales-comm/sales-comm.component';
import { NgxSpinnerModule } from "ngx-spinner";
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    PaymentComponent,
    LoginComponent,
    SidenavComponent,
    HeaderComponent,
    LoginLayoutComponent,
    HomeLayoutComponent,
    UserpostComponent,
    MypostComponent,
    FooterComponent,
    UserRoleDirective,
    UserDirective,
    FavouriteComponent,
    PromotionComponent,
    SalesContestComponent,
    NewProductComponent,
    PriceChangeComponent,
    SuperMarketComponent,
    NotificationComponent,
    SalesCommComponent
  ],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatCheckboxModule,
    A11yModule,
    ClipboardModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    // MatSortModule,
    // MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
    ChartsModule,
    HttpModule,
    AgGridModule.withComponents([]),
    HttpClientModule,
    ModalModule,
    ToastrModule.forRoot(),
    NgxLoadingModule.forRoot({}),
    OverlayModule,
    NgImageSliderModule,
    NgMultiSelectDropDownModule.forRoot(),
    TagInputModule,
    GalleryModule,
    LightboxModule,
    ImageViewerModule,
    NgxSpinnerModule,
    NgbModule
    //HttpClient
  ],
  exports: [
    UserDirective,
    UserRoleDirective
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  providers: [AuthGuard, LoginService, DatePipe, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
