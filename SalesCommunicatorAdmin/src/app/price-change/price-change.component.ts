import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import 'rxjs/add/operator/map';
import { MyPostService } from '../_services/my-post.service';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from '../_modal';
import { Gallery, GalleryItem, ThumbnailsPosition, ImageSize } from "ng-gallery";
import { Lightbox } from "ng-gallery/lightbox";
import { post } from 'jquery';

@Component({
  selector: 'app-price-change',
  templateUrl: './price-change.component.html',
  styleUrls: ['./price-change.component.css']
})
export class PriceChangeComponent implements OnInit {
  userPostList: string[]; privacyType: string; PostType: string; publishedDate: Date; createdBy: string;
  MediaFiles: string[]; Commands: string; enddate: string; startdate: string; resultArray: {}; postMediaList = [];
  tagName: string; myJsonString: any; click: boolean = false;
  UserRole: string;
  items: GalleryItem[];
  @ViewChild("itemTemplate", { static: true }) itemTemplate: TemplateRef<any>;
  constructor(private _UserPost: MyPostService, private toastr: ToastrService, private modalService: ModalService, public gallery: Gallery, public lightbox: Lightbox) { }

  ngOnInit() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
  }
searchuserData() {
    this.LoadUserPostData(this.startdate, this.enddate);
  }

  private LoadUserPostData(FromDate: string, ToDate: string) {
    //alert(this.PostType);
    this.UserRole = sessionStorage.getItem("User");
    var _fromDt = null;
    var _toDt = null;
    var _Privacy = null;
    var _posttype = '';
    var _CreatedBy = null;
    var _tagNam = null;
    var _UserName = null;
    if (this.PostType == null || this.PostType == undefined) {
      _posttype = '';
    }
    else {
      _posttype = this.PostType;
    }
    if (this.tagName == null || this.tagName == undefined) {
      _tagNam = null;
    }
    else {
      _tagNam = this.tagName;
    }

    if (sessionStorage.getItem("UserName") != null || sessionStorage.getItem("UserName") != undefined) {
      _UserName = sessionStorage.getItem("UserName");
    }
    this._UserPost.registerUsers(_CreatedBy, _Privacy, '3', FromDate, ToDate, _tagNam, _UserName, this.UserRole).subscribe(
      _UserPostDt => {
        this.userPostList = JSON.parse(JSON.stringify(_UserPostDt)).Payload;
        console.log(this.userPostList);
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }

  makepostFav(postNo) {
    var PostNo = String(postNo);
    var UserName=this.UserRole;
    this._UserPost.MakePostFav(PostNo,UserName).subscribe(
      _UserPostDt => {
        this.click = true;
        this.toastr.success("You love this post", "success");
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }

  reset() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
    this.PostType = '';
    this.tagName = '';
  }
}
