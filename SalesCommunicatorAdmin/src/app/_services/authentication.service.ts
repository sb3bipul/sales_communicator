import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import { SCUser } from '../_models/user'
import { Role } from '../_models/role';

@Injectable()
export class AuthenticationService {
    private user: SCUser;
    constructor(private http: Http) { }
    
    isAuthorized() {
        return !!this.user;
    }

    hasRole(role: Role) {
        return this.isAuthorized() && this.user.role === role;
    }

    login(role: Role) {
        this.user = { role: role };
    }

    logout() {
        this.user = null;
    }
}