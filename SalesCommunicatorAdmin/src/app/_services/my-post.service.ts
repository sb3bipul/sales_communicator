import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { MyPost, MyPostData, PostMedia } from '../_models/my-post';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { appConfig } from '../app.config';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MyPostService {
  private baseurl: string = appConfig.apiUrl;
  constructor(private http: Http, private httpClient: HttpClient) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  insertUserPostData(_CreatedBy: string, _PrivacyTypeId: number, _PostTypeId: number, _Commends: string, _fromDate: Date, _toDate: Date, _Header: string, _CompanyId: string, _Division: string, _Group: string,_TagLocation: string): Observable<MyPost> {
    var obj = { CREATEDBY: _CreatedBy, PRIVACYTYPEID: _PrivacyTypeId, POSTTYPEID: _PostTypeId, COMMENTS: _Commends, FROMDATE: _fromDate, TODATE: _toDate, Header: _Header, CompanyId: _CompanyId, DivisionId: _Division, GroupId: _Group,Zone: _TagLocation }
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
    let body = JSON.stringify(obj);//this.serializeObj(obj);
    return this.http.post(this.baseurl + 'api/MyPost/InsertUserPostData', body, options)
      .map((res: Response) => {
        //console.log('Insert user post data: ' + JSON.stringify(res));
        return res.json();
      })
      .catch(this.handleError);
  }

  // insertUserPostMediaData(_PostNo: string, _MediaName: string, _MediaBase64: string, _ct: string): Observable<PostMedia> {
  //   var obj = { POSTNO: _PostNo, MEDIAFILENAME: _MediaName, MediaBase64: _MediaBase64, ct: _ct }
  //   let headers = new Headers({ 'Content-Type': 'application/json' });
  //   let options = new RequestOptions({ method: RequestMethod.Post, headers: headers });
  //   let body = JSON.stringify(obj);//this.serializeObj(obj);
  //   return this.http.post(this.baseurl + 'api/MyPost/InsertUserPostMediaData', body, options)
  //     .map((res: Response) => {
  //       console.log('Insert user post data: ' + JSON.stringify(res));
  //       return res.json();
  //     })
  //     .catch(this.handleError);
  // }

  public insertUserPostMediaData(_PostNo: string, _MediaName: string, _MediaBase64: string, _pdf64: string, _pdfName: string, _excelName: string, _excelBase64: string, _pptName: string, _pptBase64: string, _ct: string) {
    var obj = { POSTNO: _PostNo, MEDIAFILENAME: _MediaName, MediaBase64: _MediaBase64, PdfBase64: _pdf64, pdfName: _pdfName, excelFileName: _excelName, excelFileData: _excelBase64, pptFileName: _pptName, pptFileData: _pptBase64, ct: _ct }
    return this.httpClient.post(this.baseurl + 'api/MyPost/InsertUserPostMediaData', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  public registerUsers(_CreatedBy: string, _PrivacyTypeId: string, _PostTypeId: string, _fromdate: string, _ToDate: string, _TagText: string, _UserName: string, _UserRole) {
    var obj = { CREATEDBY: _CreatedBy, FROMDATE: _fromdate, TODATE: _ToDate, PRIVACYTYPEID: _PrivacyTypeId, POSTTYPEID: _PostTypeId, TagText: _TagText, UserName: _UserName, UserRole: _UserRole }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetUserPoseMediadata', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  public registerUsersPost(_CreatedBy: string, _PrivacyTypeId: string, _PostTypeId: string, _fromdate: string, _ToDate: string, _TagText: string, _UserName: string, _UserRole, _isFavorite) {
    var obj = { CREATEDBY: _CreatedBy, FROMDATE: _fromdate, TODATE: _ToDate, PRIVACYTYPEID: _PrivacyTypeId, POSTTYPEID: _PostTypeId, TagText: _TagText, UserName: _UserName, UserRole: _UserRole, Favorite: _isFavorite }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetUserFavoritePoseMediadata', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  public MakePostFav(_PostNo: string, _Username:string) {
    // debugger;
    var obj = { PostNo: _PostNo, Username:_Username }
    return this.httpClient.post(this.baseurl + 'api/MyPost/MakePostFavourite', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  CompanyList(_CompanyCode: string) {
    var obj = { CompanyCode: _CompanyCode }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetCompanyList', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  getGroupListByCompany(_CompanyId: string, _DivisionId: string, _GroupId: string) {
    var obj = { CompanyId: _CompanyId, DivisionId: _DivisionId, GroupId: _GroupId }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetGroupByCompany', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  getDivisionByCompany(_CompanyId: string, _DivisionId: string) {
    var obj = { CompanyId: _CompanyId, DivisionId: _DivisionId }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetDivisionByCompany', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  getAllZone(_CompanyId: string) {
    var obj = { CompanyId: _CompanyId }
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetAllZone', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  InsertUserPostTag(_POstNo: string, _UserName: string, _TagLocation: string) {
    var obj = { POstNo: _POstNo, UserName: _UserName, TagLocation: _TagLocation }
    return this.httpClient.post(this.baseurl + 'api/MyPost/InsertUserPostTag', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  InsertUserPostLocation(_POstNo: string, _TagLocation: string) {
    var obj = { POSTNO: _POstNo, POSTLOCATION: _TagLocation }
    return this.httpClient.post(this.baseurl + 'api/MyPost/InsertUserPostLocation', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }
  GetUserListByZoneGroupDiv(_zone:string,_groupId:string,_DivisionId:string){
    var obj={Zone:_zone,GroupId:_groupId,DivisionId:_DivisionId}
    return this.httpClient.post(this.baseurl+'api/MyPost/GetUserListByZoneGroupDiv',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }
  GetUserPostDataNotification(_userId:string){
    var obj={UserName:_userId}
    return this.httpClient.post(this.baseurl + 'api/MyPost/GetUserPoseMediadataNotification',obj,{
      headers:new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data=>data);
  }

  AddComment(_PostNo:string,_SenderId:string,_ReceiverId:string,_Comment:string,_CompanyId:string){
    var obj={PostNo:_PostNo,SenderId:_SenderId,ReceiverId:_ReceiverId,Comment:_Comment,CompanyId:_CompanyId}
    return this.httpClient.post(this.baseurl+'api/MyPost/Postcomment',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }

  SaveCommentCount(_PostNo:string,_UserName:string){
    var obj={PostNo:_PostNo,UserName:_UserName}
    return this.httpClient.post(this.baseurl+'api/MyPost/Getsavecommentcount',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }

  ReplyComment(_PostNo:string,_SenderId:string,_ReceiverId:string,_Comment:string){
    var obj={PostNo:_PostNo,SenderUser:_SenderId,ReceiverUser:_ReceiverId,TextMsg:_Comment}
    return this.httpClient.post(this.baseurl+'api/MyPost/replycomment',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }

  CommentListByUser(_postNo:string,_userName:string)
  {
    var obj={PostNo:_postNo,UserName:_userName}
    return this.httpClient.post(this.baseurl+'api/MyPost/PostcommentList',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }

  CommentedListUser(_postNo:string){
    var obj={PostNo:_postNo}
    return this.httpClient.post(this.baseurl+'api/MyPost/Commenteduserlist',obj,{
      headers:new HttpHeaders({
        'Content-Type':'application/json'
      })
    })
  }
}
