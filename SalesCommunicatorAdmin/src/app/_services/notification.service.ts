import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs';
import { appConfig } from '../app.config';
import { notification, Count } from '../_models/notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private baseurl: string = appConfig.apiUrl;
  constructor(private http: Http, private httpClient: HttpClient) { }

  private handleError(error: any) {
    var applicationError = error.headers.get('Application-Error');
    var serverError = error.json();
    var modelStateErrors: string = '';
    if (!serverError.type) {
      console.log(serverError);
      for (var key in serverError) {
        if (serverError[key])
          modelStateErrors += serverError[key] + '\n';
      }
    }
    modelStateErrors = modelStateErrors = '' ? null : modelStateErrors;
    return Observable.throw(applicationError || modelStateErrors || 'Server error');
  }

  InsertNewNotification(notification: Notification): Observable<notification> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.post<notification>(this.baseurl + 'api/Notification/InsertNotification/', notification, httpOptions);
  }
  UpdateNotification(userName: string) {
    var obj = { userid: userName }
    return this.httpClient.post(this.baseurl + 'api/Notification/UpdateNotification/', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  DeleteNotification(userid: string, notificationId: string): Observable<number> {
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };
    return this.httpClient.post<number>(this.baseurl + 'api/Notification/DeleteNotification/', httpOptions);
  }

  getNotificationList(_userid: number) {
    var obj = { userid: _userid }
    return this.httpClient.post(this.baseurl + 'api/Notification/GetNotification', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }

  getNotificationCount(_userName: string) {
    var obj = { UserId: _userName }
    return this.httpClient.post(this.baseurl + 'api/Notification/getNotificationCount', obj, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    }).map(data => data);
  }
}