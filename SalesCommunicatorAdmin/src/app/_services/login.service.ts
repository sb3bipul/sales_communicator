import { Injectable } from '@angular/core';
//import { Http, Headers, RequestOptions, Response, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { appConfig } from '../app.config';
import { ILogin, User } from '../_models/login';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SCUser } from '../_models/user'
import { Role } from '../_models/role';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private baseurl: string = appConfig.loginapi;
  private user: SCUser;
  userData = new BehaviorSubject<User>(new User());
  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  get isLoggedIn(): boolean {
    return (localStorage.getItem('authToken') !== null);
  }
  constructor(private http: HttpClient, private router: Router) { }

  login(userDetails, role: Role) {
    var loginUrl = this.baseurl + 'api/login';
    this.user = { role: role };
    return this.http.post<any>(loginUrl, userDetails).pipe(map(response => {
      localStorage.setItem('authToken', response.token);
      sessionStorage.setItem('roleId', response.userDetails.RoleId);
      sessionStorage.setItem('FullName', response.userDetails.UserFullName);
      sessionStorage.setItem('UserName', response.userDetails.UserName);
      sessionStorage.setItem('User', response.userDetails.Role);
      this.setUserDetails();
      return response;
    }));
  }

  setUserDetails() {
    if (localStorage.getItem('authToken')) {
      const userDetails = new User();
      const decodeUserDetails = JSON.parse(window.atob(localStorage.getItem('authToken').split('.')[1]));
      userDetails.UserName = decodeUserDetails.sub;
      userDetails.isLoggedIn = true;
      this.userData.next(userDetails);
    }
  }

  logout() {
    localStorage.removeItem('authToken');
    sessionStorage.removeItem('roleId');
    sessionStorage.removeItem('FullName');
    sessionStorage.removeItem('UserName');    
    sessionStorage.clear();
    this.userData.next(new User());
    this.router.navigate(['/login']);
    this.user = null;
  }

  isAuthorized() {
    return !!this.user;
  }

  hasRole(role: Role) {
    return this.isAuthorized() && this.user.role === role;
  }
}
