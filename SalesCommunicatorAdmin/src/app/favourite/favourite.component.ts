import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Gallery, GalleryItem } from 'ng-gallery';
import { Lightbox } from 'ng-gallery/lightbox';
import { ToastrService } from 'ngx-toastr';
import { delay } from 'rxjs-compat/operator/delay';
import { ModalService } from '../_modal';
import { MyPostService } from '../_services/my-post.service';

@Component({
  selector: 'app-favourite',
  templateUrl: './favourite.component.html',
  styleUrls: ['./favourite.component.css']
})
export class FavouriteComponent implements OnInit {
  public loading:boolean=false;
  userPostList: string[]; privacyType: string; PostType: string; publishedDate: Date; createdBy: string;
  MediaFiles: string[]; Commands: string; enddate: string; startdate: string; resultArray: {}; postMediaList = [];
  tagName: string; myJsonString: any;
  UserRole: string;
  items: GalleryItem[];
  counter: number = 0;
  showModal: boolean;
  imgUrl:string[];
  currImgUrl:string;
  selectedindex: number = 0;
  @ViewChild("itemTemplate", { static: true }) itemTemplate: TemplateRef<any>;
  constructor(private _UserPost: MyPostService, private toastr: ToastrService, private modalService: ModalService, public gallery: Gallery, public lightbox: Lightbox) { }

  ngOnInit() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
  }

  onNext() {
    if (this.counter != this.imgUrl.length - 1) {
      this.counter++;
    }
  }

  onPrevious() {
    if (this.counter > 0) {
      this.counter--;
    }
  }
  
  hide()
  {
    this.showModal = false;
    this.imgUrl=[];
  }
  openImg1(i:any,j:any){    
    this.counter=i;
    this.imgUrl=j;
    this.showModal=true;
  }
  openImg(i:any){    
    this.currImgUrl=i;
    this.showModal=true;
  }
  searchuserData() {
    this.LoadUserPostData(this.startdate, this.enddate);
  }

  private LoadUserPostData(FromDate: string, ToDate: string) {    
    this.loading=true;
    
    //alert(this.PostType);
    this.UserRole = sessionStorage.getItem("User");
    //this.UserRole = sessionStorage.getItem("roleId");
    var _fromDt = null;
    var _toDt = null;
    var _Privacy = null;
    var _posttype = '';
    var _CreatedBy = null;
    var _tagNam = null;
    var _UserName = null;
    if (this.PostType == null || this.PostType == undefined) {
      _posttype = '';
    }
    else {
      _posttype = this.PostType;
    }
    if (this.tagName == null || this.tagName == undefined) {
      _tagNam = null;
    }
    else {
      _tagNam = this.tagName;
    }

    if (sessionStorage.getItem("UserName") != null || sessionStorage.getItem("UserName") != undefined) {
      _UserName = sessionStorage.getItem("UserName");
    }
    this._UserPost.registerUsersPost(_CreatedBy, _Privacy, _posttype, FromDate, ToDate, _tagNam, _UserName, this.UserRole,1).subscribe(
      _UserPostDt => {
        this.userPostList = JSON.parse(JSON.stringify(_UserPostDt)).Payload;
        console.log("favData",this.userPostList);
        this.loading=false;
      },
      error => {
        console.log("Failed to load user post media data", error);
      }
    );
  }

  reset() {
    var stDt = new Date();
    var Todt = new Date();
    stDt.setDate(stDt.getDate() - 30);
    Todt.setDate(Todt.getDate() + 2);
    this.startdate = stDt.toISOString().split('T')[0];
    this.enddate = Todt.toISOString().split('T')[0];
    this.LoadUserPostData(this.startdate, this.enddate);
    this.PostType = '';
    this.tagName = '';
  }
}
